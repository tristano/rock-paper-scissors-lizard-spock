package nl.ordina.oddsevensgameengine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameSessionConverterTest {

    private GameSessionConverter converter;

    @BeforeEach
    void setUp() {
        converter = new GameSessionConverter();
        GameSessionConverter.RoundConverter roundConverter = new GameSessionConverter.RoundConverter();
        roundConverter.moveConverter = new GameSessionConverter.MoveConverter();
        converter.playerConverter = new GameSessionConverter.PlayerConverter();
        converter.roundConverter = roundConverter;
        converter.resultConverter = new GameSessionConverter.ResultConverter();
        converter.gameSessionStatusConverter = new GameSessionConverter.GameSessionStatusConverter();
    }

    @Test
    @DisplayName("Converts from GameSession to GameSessionEntity")
    void testConvertGameSessionToGameSessionEntity() {
        DTOs.GameSession gameSession = new DTOs.GameSession();
        gameSession.sessionUUID = "clientUUID";
        gameSession.player1 = new DTOs.Player();
        gameSession.player2 = new DTOs.Player();
        gameSession.status = DTOs.GameSessionStatus.CANCELLED;
        gameSession.roundInProgress = new DTOs.Round();
        gameSession.lastRound = new DTOs.Round();
        gameSession.resultLastRound = DTOs.Result.PLAYER_1_WINS;

        GameSessionEntity gameSessionEntity = converter.convert(gameSession);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals("clientUUID", gameSessionEntity.getSessionUUID()),
                () -> assertEquals(GameSessionEntity.SessionStatusEmbedded.CANCELLED, gameSessionEntity.getStatus()),
                () -> assertEquals(GameSessionEntity.ResultEmbedded.PLAYER_1_WINS, gameSessionEntity.getResultLastRound()));
    }

    @Test
    @DisplayName("Converts from GameSession to GameSessionEntity")
    void testConvertGameSessionEntityToGameSession() {
        GameSessionEntity gameSessionEntity = new GameSessionEntity();
        gameSessionEntity.setSessionUUID("uuid");
        gameSessionEntity.setPlayer1(new GameSessionEntity.PlayerEmbedded());
        gameSessionEntity.setPlayer2(new GameSessionEntity.PlayerEmbedded());
        gameSessionEntity.setStatus(GameSessionEntity.SessionStatusEmbedded.CANCELLED);
        gameSessionEntity.setRoundInProgress(new GameSessionEntity.RoundEmbedded());
        gameSessionEntity.setLastRound(new GameSessionEntity.RoundEmbedded());
        gameSessionEntity.setResultLastRound(GameSessionEntity.ResultEmbedded.PLAYER_1_WINS);

        DTOs.GameSession gameSession = converter.convert(gameSessionEntity);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals("uuid", gameSession.sessionUUID),
                () -> assertEquals(DTOs.GameSessionStatus.CANCELLED, gameSession.status),
                () -> assertEquals(DTOs.Result.PLAYER_1_WINS, gameSession.resultLastRound));
    }

    @Test
    @DisplayName("Converts empty objects without generating NullPointerExceptions")
    void testConvertEmptyObjects() {
        converter.convert(new DTOs.GameSession());
        converter.convert(new GameSessionEntity());
    }

	static class GameSessionStatusConverterTest {
		private GameSessionConverter.GameSessionStatusConverter converter;
		private static final int NUMBER_OF_STATUSES = 6;

		@BeforeEach
		void setup() {
			converter = new GameSessionConverter.GameSessionStatusConverter();
		}

		@Test
		@DisplayName("Converts from GameSessionStatus to GameSessionStatusEmbedded")
		void testConvertSessionStatusToSessionStatusEmbedded() {
			Assertions.assertAll("It should return the right results",
					() -> assertEquals(GameSessionEntity.SessionStatusEmbedded.NEW, converter.convert(DTOs.GameSessionStatus.NEW)),
					() -> assertEquals(GameSessionEntity.SessionStatusEmbedded.NEW_ROUND, converter.convert(DTOs.GameSessionStatus.NEW_ROUND)),
					() -> assertEquals(GameSessionEntity.SessionStatusEmbedded.TERMINATED, converter.convert(DTOs.GameSessionStatus.TERMINATED)),
					() -> assertEquals(GameSessionEntity.SessionStatusEmbedded.CANCELLED, converter.convert(DTOs.GameSessionStatus.CANCELLED)),
					() -> assertEquals(GameSessionEntity.SessionStatusEmbedded.WAITING_FOR_PLAYER1_CHOICE, converter.convert(DTOs.GameSessionStatus.WAITING_FOR_PLAYER1_CHOICE)),
					() -> assertEquals(GameSessionEntity.SessionStatusEmbedded.WAITING_FOR_PLAYER2_CHOICE, converter.convert(DTOs.GameSessionStatus.WAITING_FOR_PLAYER2_CHOICE)));
		}

		@Test
		@DisplayName("Converts from GameSessionStatusEmbedded to GameSessionStatus")
		void testConvertSessionStatusEmbeddedToSessionStatus() {
			Assertions.assertAll("It should return the right results",
					() -> assertEquals(DTOs.GameSessionStatus.NEW, converter.convert(GameSessionEntity.SessionStatusEmbedded.NEW)),
					() -> assertEquals(DTOs.GameSessionStatus.NEW_ROUND, converter.convert(GameSessionEntity.SessionStatusEmbedded.NEW_ROUND)),
					() -> assertEquals(DTOs.GameSessionStatus.TERMINATED, converter.convert(GameSessionEntity.SessionStatusEmbedded.TERMINATED)),
					() -> assertEquals(DTOs.GameSessionStatus.CANCELLED, converter.convert(GameSessionEntity.SessionStatusEmbedded.CANCELLED)),
					() -> assertEquals(DTOs.GameSessionStatus.WAITING_FOR_PLAYER1_CHOICE, converter.convert(GameSessionEntity.SessionStatusEmbedded.WAITING_FOR_PLAYER1_CHOICE)),
					() -> assertEquals(DTOs.GameSessionStatus.WAITING_FOR_PLAYER2_CHOICE, converter.convert(GameSessionEntity.SessionStatusEmbedded.WAITING_FOR_PLAYER2_CHOICE)));
		}

		@Test
		@DisplayName("Amount of different statuses is known and unchanged")
		void testAmountDifferentStatusesKnownAndUnchanged() {
			Assertions.assertAll("Amount of different statuses is known and unchanged",
					() -> assertEquals(NUMBER_OF_STATUSES, GameSessionEntity.SessionStatusEmbedded.values().length),
					() -> assertEquals(NUMBER_OF_STATUSES, DTOs.GameSessionStatus.values().length));
		}
	}

	static class MoveConverterTest {
		private GameSessionConverter.MoveConverter converter;
		private static final int NUMBER_OF_MOVES = 5;

		@BeforeEach
		void setup() {
			converter = new GameSessionConverter.MoveConverter();
		}

		@Test
		@DisplayName("Converts from Move to MoveEmbedded")
		void testConvertMoveToMoveEmbedded() {
			Assertions.assertAll("It should return the right results",
					() -> assertEquals(GameSessionEntity.MoveEmbedded.ONE, converter.convert(DTOs.Move.ONE)),
					() -> assertEquals(GameSessionEntity.MoveEmbedded.TWO, converter.convert(DTOs.Move.TWO)),
					() -> assertEquals(GameSessionEntity.MoveEmbedded.THREE, converter.convert(DTOs.Move.THREE)),
					() -> assertEquals(GameSessionEntity.MoveEmbedded.FOUR, converter.convert(DTOs.Move.FOUR)),
					() -> assertEquals(GameSessionEntity.MoveEmbedded.FIVE, converter.convert(DTOs.Move.FIVE)));
		}

		@Test
		@DisplayName("Converts from MoveEmbedded to Move")
		void testConvertMoveEmbeddedToMove() {
			Assertions.assertAll("It should return the right results",
					() -> assertEquals(DTOs.Move.ONE, converter.convert(GameSessionEntity.MoveEmbedded.ONE)),
					() -> assertEquals(DTOs.Move.TWO, converter.convert(GameSessionEntity.MoveEmbedded.TWO)),
					() -> assertEquals(DTOs.Move.THREE, converter.convert(GameSessionEntity.MoveEmbedded.THREE)),
					() -> assertEquals(DTOs.Move.FOUR, converter.convert(GameSessionEntity.MoveEmbedded.FOUR)),
					() -> assertEquals(DTOs.Move.FIVE, converter.convert(GameSessionEntity.MoveEmbedded.FIVE)));
		}

		@Test
		@DisplayName("Amount of different statuses is known and unchanged")
		void testAmountDifferentStatusesKnownAndUnchanged() {
			Assertions.assertAll("Amount of different moves is known and unchanged",
					() -> assertEquals(NUMBER_OF_MOVES, GameSessionEntity.MoveEmbedded.values().length),
					() -> assertEquals(NUMBER_OF_MOVES, DTOs.Move.values().length));
		}
	}

	static class PlayerConverterTest {

		private GameSessionConverter.PlayerConverter converter;

		@BeforeEach
		void setUp() {
			converter = new GameSessionConverter.PlayerConverter();
		}

		@Test
		@DisplayName("Converts from Player to PlayerEmbedded")
		void testConvertPlayerToPlayerEmbedded() {
			DTOs.Player player = new DTOs.Player();
			player.score = 10;
			player.name = "Jan";
			player.clientUUID = "aluelueluelue";

			GameSessionEntity.PlayerEmbedded playerEmbedded = converter.convert(player);

			Assertions.assertAll("It should give the right results",
					() -> assertEquals("Jan", playerEmbedded.getName()),
					() -> assertEquals(10, playerEmbedded.getScore()),
					() -> assertEquals("aluelueluelue", playerEmbedded.getClientUUID()));
		}

		@Test
		@DisplayName("Converts from PlayerEmbedded to Player")
		void testConvertPlayerEmbeddedToPlayer() {
			GameSessionEntity.PlayerEmbedded playerEmbedded = new GameSessionEntity.PlayerEmbedded();
			playerEmbedded.setScore(10);
			playerEmbedded.setName("Jan");
			playerEmbedded.setClientUUID("aluelueluelue");

			DTOs.Player player = converter.convert(playerEmbedded);

			Assertions.assertAll("It should give the right results",
					() -> assertEquals("Jan", player.name),
					() -> assertEquals(10, player.score),
					() -> assertEquals("aluelueluelue", player.clientUUID));
		}

		@Test
		@DisplayName("Converts empty objects without generating NullPointerExceptions")
		void testConvertEmptyObjects() {
			converter.convert(new DTOs.Player());
			converter.convert(new GameSessionEntity.PlayerEmbedded());
		}
	}

	static class ResultConverterTest {
		private GameSessionConverter.ResultConverter converter;
		private static final int NUMBER_OF_RESULTS = 2;

		@BeforeEach
		void setup() {
			converter = new GameSessionConverter.ResultConverter();
		}

		@Test
		@DisplayName("Converts from Result to ResultEmbedded")
		void testConvertResultToResultEmbedded() {
			Assertions.assertAll("It should return the right results",
					() -> assertEquals(GameSessionEntity.ResultEmbedded.PLAYER_1_WINS, converter.convert(DTOs.Result.PLAYER_1_WINS)),
					() -> assertEquals(GameSessionEntity.ResultEmbedded.PLAYER_2_WINS, converter.convert(DTOs.Result.PLAYER_2_WINS)));
		}

		@Test
		@DisplayName("Converts from ResultEmbedded to Result")
		void testConvertResultEmbeddedToResult() {
			Assertions.assertAll("It should return the right results",
					() -> assertEquals(DTOs.Result.PLAYER_1_WINS, converter.convert(GameSessionEntity.ResultEmbedded.PLAYER_1_WINS)),
					() -> assertEquals(DTOs.Result.PLAYER_2_WINS, converter.convert(GameSessionEntity.ResultEmbedded.PLAYER_2_WINS)));
		}

		@Test
		@DisplayName("Amount of different statuses is known and unchanged")
		void testAmountDifferentStatusesKnownAndUnchanged() {
			Assertions.assertAll("Amount of different results is known and unchanged",
					() -> assertEquals(NUMBER_OF_RESULTS, GameSessionEntity.ResultEmbedded.values().length),
					() -> assertEquals(NUMBER_OF_RESULTS, DTOs.Result.values().length));
		}
	}

	static class RoundConverterTest {

		private GameSessionConverter.RoundConverter converter;
		private GameSessionConverter.MoveConverter moveConverter;

		@BeforeEach
		void setUp() {
			converter = new GameSessionConverter.RoundConverter();
			moveConverter = new GameSessionConverter.MoveConverter();
			converter.moveConverter = moveConverter;
		}

		@Test
		@DisplayName("Converts from Round to RoundEmbedded")
		void testConvertRoundToRoundEmbedded() {
			DTOs.Round roundDTO = new DTOs.Round();
			roundDTO.movePlayer1 = DTOs.Move.ONE;
			roundDTO.movePlayer2 = DTOs.Move.THREE;

			GameSessionEntity.RoundEmbedded roundEmbedded = converter.convert(roundDTO);

			Assertions.assertAll("It should give the right results",
					() -> assertEquals(GameSessionEntity.MoveEmbedded.ONE, roundEmbedded.getMovePlayer1()),
					() -> assertEquals(GameSessionEntity.MoveEmbedded.THREE, roundEmbedded.getMovePlayer2()));
		}

		@Test
		@DisplayName("Converts from RoundEmbbedded to Round")
		void testConvertRoundEmbeddedToRound() {
			GameSessionEntity.RoundEmbedded roundEmbedded = new GameSessionEntity.RoundEmbedded();
			roundEmbedded.setMovePlayer1(GameSessionEntity.MoveEmbedded.ONE);
			roundEmbedded.setMovePlayer2(GameSessionEntity.MoveEmbedded.THREE);

			DTOs.Round round = converter.convert(roundEmbedded);

			Assertions.assertAll("It should give the right results",
					() -> assertEquals(DTOs.Move.ONE, round.movePlayer1),
					() -> assertEquals(DTOs.Move.THREE, round.movePlayer2));
		}

		@Test
		@DisplayName("Converts empty objects without generating NullPointerExceptions")
		void testConvertEmptyObjects() {
			converter.convert(new DTOs.Round());
			converter.convert(new GameSessionEntity.RoundEmbedded());
		}
	}
}
