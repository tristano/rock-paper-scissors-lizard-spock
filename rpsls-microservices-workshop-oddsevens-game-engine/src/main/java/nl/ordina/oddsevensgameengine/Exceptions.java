package nl.ordina.oddsevensgameengine;

public class Exceptions {
	public static class NoSuchGameSessionException extends RuntimeException {
	}

	public static class PlayerNotAllowedToSendAChoiceException extends RuntimeException {
	}

	public static class PlayerNotInGameSessionException extends RuntimeException {
	}

	public static class SessionIdsDoNotMatchException extends RuntimeException {
	}
}
