package nl.ordina.oddsevensgameengine;

import io.smallrye.reactive.messaging.annotations.Broadcast;
import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DashboardResultsProducer {
    @Inject
    @Broadcast
    @Channel(value = "dashboard-results")
    Emitter<DTOs.FinalResult> eventEmitter;

    public void sendEvent(DTOs.FinalResult finalResult) {
        eventEmitter.send(finalResult);
    }
}
