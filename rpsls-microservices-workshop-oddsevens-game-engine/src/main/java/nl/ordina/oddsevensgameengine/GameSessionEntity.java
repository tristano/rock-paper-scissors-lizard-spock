package nl.ordina.oddsevensgameengine;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection = "oddsevens.sessions")
public class GameSessionEntity extends PanacheMongoEntity {
    private String sessionUUID;
    private PlayerEmbedded player1;
    private PlayerEmbedded player2;
    private SessionStatusEmbedded status;
    private RoundEmbedded roundInProgress;
	private RoundEmbedded lastRound;
    private ResultEmbedded resultLastRound;

    public String getSessionUUID() {
        return sessionUUID;
    }

    public void setSessionUUID(String sessionUUID) {
        this.sessionUUID = sessionUUID;
    }

    public PlayerEmbedded getPlayer1() {
        return player1;
    }

    public void setPlayer1(PlayerEmbedded player1) {
        this.player1 = player1;
    }

    public PlayerEmbedded getPlayer2() {
        return player2;
    }

    public void setPlayer2(PlayerEmbedded player2) {
        this.player2 = player2;
    }

    public SessionStatusEmbedded getStatus() {
        return status;
    }

    public void setStatus(SessionStatusEmbedded status) {
        this.status = status;
    }

    public RoundEmbedded getRoundInProgress() {
        return roundInProgress;
    }

    public void setRoundInProgress(RoundEmbedded roundInProgress) {
        this.roundInProgress = roundInProgress;
    }

    public ResultEmbedded getResultLastRound() {
        return resultLastRound;
    }

    public void setResultLastRound(ResultEmbedded resultLastRound) {
        this.resultLastRound = resultLastRound;
    }

	public RoundEmbedded getLastRound() {
		return lastRound;
	}

	public void setLastRound(RoundEmbedded lastRound) {
		this.lastRound = lastRound;
	}

	public enum MoveEmbedded {
		ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);

		private int numericValue;

		MoveEmbedded (int numericValue) {
			this.numericValue = numericValue;
		}

		public int getNumericValue() {
			return numericValue;
		}
	}

	public static class PlayerEmbedded {
		private String name;
		private String clientUUID;
		private int score;

		public void incrementScore() {
			score += 1;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getClientUUID() {
			return clientUUID;
		}

		public void setClientUUID(String clientUUID) {
			this.clientUUID = clientUUID;
		}

		public int getScore() {
			return score;
		}

		public void setScore(int score) {
			this.score = score;
		}
	}

	public enum ResultEmbedded {
		PLAYER_1_WINS, PLAYER_2_WINS
	}

	public static class RoundEmbedded {
		private MoveEmbedded movePlayer1;
		private MoveEmbedded movePlayer2;

		public MoveEmbedded getMovePlayer1() {
			return movePlayer1;
		}

		public void setMovePlayer1(MoveEmbedded movePlayer1) {
			this.movePlayer1 = movePlayer1;
		}

		public MoveEmbedded getMovePlayer2() {
			return movePlayer2;
		}

		public void setMovePlayer2(MoveEmbedded movePlayer2) {
			this.movePlayer2 = movePlayer2;
		}
	}

	public enum SessionStatusEmbedded {
		NEW, NEW_ROUND, CANCELLED, TERMINATED, WAITING_FOR_PLAYER1_CHOICE, WAITING_FOR_PLAYER2_CHOICE
	}
}
