package nl.ordina.oddsevensgameengine;

import io.smallrye.reactive.messaging.annotations.Broadcast;
import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GameEventsProducer {
    @Inject
    @Broadcast
    @Channel(value = "oddsevens-game-events")
    Emitter<DTOs.GameEvent> eventEmitter;

    public void sendEvent(DTOs.GameEvent gameEvent) {
        eventEmitter.send(gameEvent);
    }
}
