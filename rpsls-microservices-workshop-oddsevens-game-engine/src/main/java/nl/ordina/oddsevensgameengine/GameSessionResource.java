package nl.ordina.oddsevensgameengine;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static nl.ordina.oddsevensgameengine.GameSessionEntity.SessionStatusEmbedded.CANCELLED;
import static nl.ordina.oddsevensgameengine.GameSessionEntity.SessionStatusEmbedded.NEW_ROUND;
import static nl.ordina.oddsevensgameengine.GameSessionEntity.SessionStatusEmbedded.TERMINATED;
import static nl.ordina.oddsevensgameengine.GameSessionEntity.SessionStatusEmbedded.WAITING_FOR_PLAYER1_CHOICE;
import static nl.ordina.oddsevensgameengine.GameSessionEntity.SessionStatusEmbedded.WAITING_FOR_PLAYER2_CHOICE;

@Path("/api/session")
public class GameSessionResource {

    @Inject
    GameSessionConverter gameSessionConverter;

    @Inject
    GameSessionRepository gameSessionRepository;

    @Inject
    GameEventsProducer gameEventsProducer;

    @Inject
    DashboardResultsProducer dashboardResultsProducer;

    private final static int SCORE_TO_WIN_THE_GAME_SESSION = 3;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public List<DTOs.GameSession> handleGetAll(@QueryParam("clientuuid") String clientUUID,
                                               @QueryParam("username") String username) {
        return getAll(clientUUID, username);
    }

    @DELETE
    @Path("/")
    public void delete() {
        deleteAllSessions();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{uuid}")
    public DTOs.GameSession get(@PathParam("uuid") String uuid) {
        return findSessionByUuid(uuid);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public DTOs.GameSession handleRegisterPlayerToSession(DTOs.Player player) {
        return registerPlayerToSession(player);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{sessionUUID}")
    public DTOs.GameSession handleSendChoice(DTOs.Choice choice, @PathParam("sessionUUID") String sessionUUID) {
        return sendChoice(choice, sessionUUID);
    }

    @DELETE
    @Path("/{sessionUUID}/status")
    public void changeStatus(@PathParam("sessionUUID") String sessionUUID) {
        cancelSession(sessionUUID);
    }

    public List<DTOs.GameSession> getAll(String clientUUID, String username) {
        if(clientUUID != null && username != null) {
            return getAllSessions(clientUUID, username).stream()
                    .filter(session -> session.getStatus() != TERMINATED)
                    .filter(session -> session.getStatus() != CANCELLED)
                    .filter(session -> session.getPlayer1().getName().equals(username) ||
                            (session.getPlayer2() != null && session.getPlayer2().getName().equals(username)))
                    .filter(session -> session.getPlayer1().getClientUUID().equals(clientUUID) ||
                            session.getPlayer2().getClientUUID().equals(clientUUID))
                    .map(gameSessionConverter::convert)
                    .collect(Collectors.toList());
        } else {
            return getAllSessions(clientUUID, username).stream()
                    .map(gameSessionConverter::convert)
                    .collect(Collectors.toList());
        }
    }

    public DTOs.GameSession findSessionByUuid(String uuid) {
        GameSessionEntity gameSessionEntity = gameSessionRepository.findBySessionUUID(uuid).
                orElseThrow(Exceptions.NoSuchGameSessionException::new);
        return gameSessionConverter.convert(gameSessionEntity);
    }

    public DTOs.GameSession registerPlayerToSession(DTOs.Player player) {
        DTOs.GameSession gameSession = gameSessionConverter.convert(registerPlayerToSession(player.name, player.clientUUID));
        if (gameSession.player2 != null && gameSession.player2.clientUUID.equals(player.clientUUID)) {
            DTOs.GameEvent gameEvent = new DTOs.GameEvent();
            gameEvent.gameEventType = DTOs.GameEventType.NEW_ROUND;
            gameEvent.sessionUUID = gameSession.sessionUUID;
            DTOs.Score score = new DTOs.Score();
            score.player1 = gameSession.player1;
            score.player2 = gameSession.player2;
            gameEvent.score = score;
            gameEvent.recipients.add(gameSession.player1);
            gameEvent.recipients.add(gameSession.player2);
            gameEventsProducer.sendEvent(gameEvent);
        }
        return gameSession;
    }

    public DTOs.GameSession sendChoice(DTOs.Choice choice, String sessionUUID) {
        if (!sessionUUID.equals(choice.sessionUUID)) {
            throw new Exceptions.SessionIdsDoNotMatchException();
        }

        DTOs.GameSession gameSession = gameSessionConverter.convert(processChoiceAndUpdateGameSession(choice));
        switch (gameSession.status) {
            case NEW_ROUND:
                DTOs.GameEvent newRoundEvent = createEvent(gameSession, DTOs.GameEventType.NEW_ROUND);
                newRoundEvent.recipients.add(gameSession.player1);
                newRoundEvent.recipients.add(gameSession.player2);
                gameEventsProducer.sendEvent(newRoundEvent);
                break;

            case TERMINATED:
                DTOs.GameEvent sessionTerminatedEvent = createEvent(gameSession, DTOs.GameEventType.SESSION_TERMINATED);
                sessionTerminatedEvent.recipients.add(gameSession.player1);
                sessionTerminatedEvent.recipients.add(gameSession.player2);
                gameEventsProducer.sendEvent(sessionTerminatedEvent);

                DTOs.FinalResult finalResult = new DTOs.FinalResult();
                finalResult.score = sessionTerminatedEvent.score;
                dashboardResultsProducer.sendEvent(finalResult);
                break;
        }

        return gameSession;
    }

    private DTOs.GameEvent createEvent(DTOs.GameSession gameSession, DTOs.GameEventType gameEventType) {
        DTOs.GameEvent gameEvent = new DTOs.GameEvent();
        gameEvent.gameEventType = gameEventType;
        gameEvent.sessionUUID = gameSession.sessionUUID;
        if (gameSession.resultLastRound == null) {
            gameEvent.score = getScoreWithoutMoves(gameSession);
        } else {
            gameEvent.score = getScoreWithMoves(gameSession);
        }
        return gameEvent;
    }

    private DTOs.PlayerWithMove getPlayer1WithMove(DTOs.GameSession gameSession) {
        DTOs.PlayerWithMove player1 = new DTOs.PlayerWithMove();
        player1.clientUUID = gameSession.player1.clientUUID;
        player1.name = gameSession.player1.name;
        player1.score = gameSession.player1.score;
        player1.move = gameSession.lastRound.movePlayer1;
        return player1;
    }

    private DTOs.PlayerWithMove getPlayer2WithMove(DTOs.GameSession gameSession) {
        DTOs.PlayerWithMove player2 = new DTOs.PlayerWithMove();
        player2.clientUUID = gameSession.player2.clientUUID;
        player2.name = gameSession.player2.name;
        player2.score = gameSession.player2.score;
        player2.move = gameSession.lastRound.movePlayer2;
        return player2;
    }

    private DTOs.Score getScoreWithMoves(DTOs.GameSession gameSession) {
        DTOs.PlayerWithMove player1 = getPlayer1WithMove(gameSession);
        DTOs.PlayerWithMove player2 = getPlayer2WithMove(gameSession);

        DTOs.Score score = new DTOs.Score();
        score.player1 = player1;
        score.player2 = player2;

        return score;
    }

    private DTOs.Score getScoreWithoutMoves(DTOs.GameSession gameSession) {
        DTOs.Score score = new DTOs.Score();
        score.player1 = gameSession.player1;
        score.player2 = gameSession.player2;
        return score;
    }

    public void cancelSession(String sessionUUID) {
        gameSessionRepository.findBySessionUUID(sessionUUID)
                .filter(session -> session.getStatus() != TERMINATED)
                .ifPresent(session -> {
                    if (session.getStatus() != GameSessionEntity.SessionStatusEmbedded.CANCELLED) {
                        setSessionAsCancelled(session);
                        DTOs.GameSession gameSession = gameSessionConverter.convert(session);
                        DTOs.GameEvent gameEvent = createEvent(gameSession, DTOs.GameEventType.SESSION_CANCELLED);
                        if (gameSession.player1 != null) {
                            gameEvent.recipients.add(gameSession.player1);
                        }
                        if (gameSession.player2 != null) {
                            gameEvent.recipients.add(gameSession.player2);
                        }
                        gameEventsProducer.sendEvent(gameEvent);
                    }
                });
    }

    public List<GameSessionEntity> getAllSessions(String clientUUID, String username) {
        return gameSessionRepository.findAll().list();
    }

    public GameSessionEntity registerPlayerToSession(String playerName, String clientUUID) {
        Optional<GameSessionEntity> session = gameSessionRepository.findNewSession();
        return session.map(value -> gameSessionRepository.updateNewSessionWithPlayerName(value, playerName, clientUUID))
                .orElseGet(() -> gameSessionRepository.makeNewSessionWithPlayerName(playerName, clientUUID));
    }

    public GameSessionEntity processChoiceAndUpdateGameSession(DTOs.Choice choice) {
        GameSessionEntity gameSessionEntity = gameSessionRepository.findBySessionUUID(choice.sessionUUID)
                .orElseThrow(Exceptions.NoSuchGameSessionException::new);

        GameSessionEntity.PlayerEmbedded player = getPlayerFromSession(gameSessionEntity, choice.clientUUID)
                .orElseThrow(Exceptions.PlayerNotInGameSessionException::new);

        checkIfPlayerIsAllowedToSendHisChoice(gameSessionEntity, player);

        GameSessionEntity.RoundEmbedded round = getOrCreateRound(gameSessionEntity);

        GameSessionEntity.MoveEmbedded move = GameSessionEntity.MoveEmbedded.valueOf(choice.move.toString());

        if (isPlayer1(player, gameSessionEntity)) {
            round.setMovePlayer1(move);
            if (!isRoundCompleted(round)) {
                gameSessionEntity.setStatus(WAITING_FOR_PLAYER2_CHOICE);
            }
        }

        if (isPlayer2(player, gameSessionEntity)) {
            round.setMovePlayer2(move);
            if (!isRoundCompleted(round)) {
                gameSessionEntity.setStatus(WAITING_FOR_PLAYER1_CHOICE);
            }
        }

        if (isRoundCompleted(round)) {
            evaluateRoundAndUpdateScore(gameSessionEntity, round);
        } else {
            gameSessionEntity.update();
            return gameSessionEntity;
        }

        if (isGameSessionCompleted(gameSessionEntity)) {
            gameSessionEntity.setStatus(TERMINATED);
        } else {
            gameSessionEntity.setStatus(NEW_ROUND);
        }
        gameSessionEntity.update();
        return gameSessionEntity;
    }

    public void deleteAllSessions() {
        gameSessionRepository.deleteAll();
    }

    public void setSessionAsCancelled(GameSessionEntity gameSessionEntity) {
        gameSessionEntity.setStatus(CANCELLED);
        gameSessionRepository.update(gameSessionEntity);
    }

    private void evaluateRoundAndUpdateScore(GameSessionEntity gameSessionEntity, GameSessionEntity.RoundEmbedded round) {
        GameSessionEntity.ResultEmbedded result = getRoundWinner(round.getMovePlayer1(), round.getMovePlayer2());
        gameSessionEntity.setResultLastRound(result);
        if (result == GameSessionEntity.ResultEmbedded.PLAYER_1_WINS) {
            gameSessionEntity.getPlayer1().incrementScore();
        }
        if (result == GameSessionEntity.ResultEmbedded.PLAYER_2_WINS) {
            gameSessionEntity.getPlayer2().incrementScore();
        }
        gameSessionEntity.setLastRound(round);
        gameSessionEntity.setRoundInProgress(null);
    }

    private GameSessionEntity.ResultEmbedded getRoundWinner(GameSessionEntity.MoveEmbedded movePlayer1, GameSessionEntity.MoveEmbedded movePlayer2) {
        return (movePlayer1.getNumericValue() + movePlayer2.getNumericValue()) % 2 == 0 ?
                GameSessionEntity.ResultEmbedded.PLAYER_1_WINS: GameSessionEntity.ResultEmbedded.PLAYER_2_WINS;
    }

    private GameSessionEntity.RoundEmbedded getOrCreateRound(GameSessionEntity gameSessionEntity) {
        GameSessionEntity.RoundEmbedded round = gameSessionEntity.getRoundInProgress();
        if (round == null) {
            round = new GameSessionEntity.RoundEmbedded();
            gameSessionEntity.setRoundInProgress(round);
        }
        return round;
    }

    private boolean isPlayer1(GameSessionEntity.PlayerEmbedded player, GameSessionEntity gameSessionEntity) {
        return player.getClientUUID().equals(gameSessionEntity.getPlayer1().getClientUUID());
    }

    private boolean isPlayer2(GameSessionEntity.PlayerEmbedded player, GameSessionEntity gameSessionEntity) {
        return player.getClientUUID().equals(gameSessionEntity.getPlayer2().getClientUUID());
    }

    private boolean isRoundCompleted(GameSessionEntity.RoundEmbedded round) {
        return round.getMovePlayer1() != null && round.getMovePlayer2() != null;
    }

    private boolean isGameSessionCompleted(GameSessionEntity gameSessionEntity) {
        return gameSessionEntity.getPlayer1().getScore() == SCORE_TO_WIN_THE_GAME_SESSION || gameSessionEntity.getPlayer2().getScore() == SCORE_TO_WIN_THE_GAME_SESSION;
    }

    private void checkIfPlayerIsAllowedToSendHisChoice(GameSessionEntity gameSessionEntity, GameSessionEntity.PlayerEmbedded player) {
        if (!Arrays.asList(WAITING_FOR_PLAYER1_CHOICE, WAITING_FOR_PLAYER2_CHOICE, NEW_ROUND).contains(gameSessionEntity.getStatus())) {
            throw new Exceptions.PlayerNotAllowedToSendAChoiceException();
        }

        if (player == gameSessionEntity.getPlayer1() && gameSessionEntity.getStatus() == WAITING_FOR_PLAYER2_CHOICE) {
            throw new Exceptions.PlayerNotAllowedToSendAChoiceException();
        }

        if (player == gameSessionEntity.getPlayer2() && gameSessionEntity.getStatus() == WAITING_FOR_PLAYER1_CHOICE) {
            throw new Exceptions.PlayerNotAllowedToSendAChoiceException();
        }
    }

    private Optional<GameSessionEntity.PlayerEmbedded> getPlayerFromSession(GameSessionEntity gameSessionEntity, String clientUUID) {
        return Stream.of(gameSessionEntity.getPlayer1(), gameSessionEntity.getPlayer2())
                .filter(player -> clientUUID.equals(player.getClientUUID()))
                .findFirst();
    }
}
