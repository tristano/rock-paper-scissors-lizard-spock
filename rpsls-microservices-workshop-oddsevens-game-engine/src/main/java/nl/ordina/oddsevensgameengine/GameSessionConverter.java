package nl.ordina.oddsevensgameengine;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;

@ApplicationScoped
public class GameSessionConverter {
    @Inject
    PlayerConverter playerConverter;

    @Inject
    RoundConverter roundConverter;

    @Inject
    ResultConverter resultConverter;

    @Inject
    GameSessionStatusConverter gameSessionStatusConverter;

    public GameSessionEntity convert(DTOs.GameSession gameSession) {
        GameSessionEntity gameSessionEntity = new GameSessionEntity();
        convert(gameSession, gameSessionEntity);
        return gameSessionEntity;
    }

    public DTOs.GameSession convert(GameSessionEntity gameSessionEntity) {
        DTOs.GameSession gameSession = new DTOs.GameSession();
        convert(gameSessionEntity, gameSession);
        return gameSession;
    }

    public void convert(DTOs.GameSession gameSession, GameSessionEntity gameSessionEntity) {
        gameSessionEntity.setSessionUUID(gameSession.sessionUUID);
        gameSessionEntity.setPlayer1(Optional.ofNullable(gameSession.player1)
                .map(playerConverter::convert)
                .orElse(null));

        gameSessionEntity.setPlayer2(Optional.ofNullable(gameSession.player2)
                .map(playerConverter::convert)
                .orElse(null));

        gameSessionEntity.setRoundInProgress(Optional.ofNullable(gameSession.roundInProgress)
                .map(roundConverter::convert)
                .orElse(null));

		gameSessionEntity.setLastRound(Optional.ofNullable(gameSession.lastRound)
				.map(roundConverter::convert)
				.orElse(null));

        gameSessionEntity.setResultLastRound(Optional.ofNullable(gameSession.resultLastRound)
                .map(resultConverter::convert)
                .orElse(null));

        gameSessionEntity.setStatus(Optional.ofNullable(gameSession.status)
                .map(gameSessionStatusConverter::convert)
                .orElse(null));
    }

    public void convert(GameSessionEntity gameSessionEntity, DTOs.GameSession gameSession) {
        gameSession.sessionUUID = gameSessionEntity.getSessionUUID();
        gameSession.player1 = Optional.ofNullable(gameSessionEntity.getPlayer1())
                .map(playerConverter::convert)
                .orElse(null);

        gameSession.player2 = Optional.ofNullable(gameSessionEntity.getPlayer2())
                .map(playerConverter::convert)
                .orElse(null);

        gameSession.roundInProgress = Optional.ofNullable(gameSessionEntity.getRoundInProgress())
                .map(roundConverter::convert)
                .orElse(null);

		gameSession.lastRound = Optional.ofNullable(gameSessionEntity.getLastRound())
				.map(roundConverter::convert)
				.orElse(null);

        gameSession.resultLastRound = Optional.ofNullable(gameSessionEntity.getResultLastRound())
                .map(resultConverter::convert)
                .orElse(null);

        gameSession.status = Optional.ofNullable(gameSessionEntity.getStatus())
                .map(gameSessionStatusConverter::convert)
                .orElse(null);
    }

	@ApplicationScoped
	public static class GameSessionStatusConverter {
		public GameSessionEntity.SessionStatusEmbedded convert(DTOs.GameSessionStatus gameSessionStatus) {
			return GameSessionEntity.SessionStatusEmbedded.valueOf(gameSessionStatus.toString());
		}

		public DTOs.GameSessionStatus convert(GameSessionEntity.SessionStatusEmbedded sessionStatusEmbedded) {
			return DTOs.GameSessionStatus.valueOf(sessionStatusEmbedded.toString());
		}
	}

	@ApplicationScoped
	public static class MoveConverter {
		public GameSessionEntity.MoveEmbedded convert(DTOs.Move move) {
			return GameSessionEntity.MoveEmbedded.valueOf(move.toString());
		}

		public DTOs.Move convert(GameSessionEntity.MoveEmbedded moveEmbedded) {
			return DTOs.Move.valueOf(moveEmbedded.toString());
		}
	}

	@ApplicationScoped
	public static class PlayerConverter {
		public GameSessionEntity.PlayerEmbedded convert(DTOs.Player player) {
			GameSessionEntity.PlayerEmbedded playerEmbedded = new GameSessionEntity.PlayerEmbedded();
			convert(player, playerEmbedded);
			return playerEmbedded;
		}

		public DTOs.Player convert(GameSessionEntity.PlayerEmbedded playerEmbedded) {
			DTOs.Player player = new DTOs.Player();
			convert(playerEmbedded, player);
			return player;
		}

		public void convert(DTOs.Player player, GameSessionEntity.PlayerEmbedded playerEmbedded) {
			playerEmbedded.setClientUUID(player.clientUUID);
			playerEmbedded.setName(player.name);
			playerEmbedded.setScore(player.score);
		}

		public void convert(GameSessionEntity.PlayerEmbedded playerEmbedded, DTOs.Player player) {
			player.clientUUID = playerEmbedded.getClientUUID();
			player.name = playerEmbedded.getName();
			player.score = playerEmbedded.getScore();
		}
	}

	@ApplicationScoped
	public static class ResultConverter {
		public GameSessionEntity.ResultEmbedded convert(DTOs.Result result) {
			return GameSessionEntity.ResultEmbedded.valueOf(result.toString());
		}

		public DTOs.Result convert(GameSessionEntity.ResultEmbedded resultEmbedded) {
			return DTOs.Result.valueOf(resultEmbedded.toString());
		}
	}

	@ApplicationScoped
	public static class RoundConverter {
		@Inject
		MoveConverter moveConverter;

		public GameSessionEntity.RoundEmbedded convert(DTOs.Round round) {
			GameSessionEntity.RoundEmbedded roundEmbedded = new GameSessionEntity.RoundEmbedded();
			convert(round, roundEmbedded);
			return roundEmbedded;
		}

		public DTOs.Round convert(GameSessionEntity.RoundEmbedded roundEmbedded) {
			DTOs.Round round = new DTOs.Round();
			convert(roundEmbedded, round);
			return round;
		}

		public void convert(DTOs.Round round, GameSessionEntity.RoundEmbedded roundEmbedded) {
			roundEmbedded.setMovePlayer1(Optional.ofNullable(round.movePlayer1)
					.map(moveConverter::convert)
					.orElse(null));

			roundEmbedded.setMovePlayer2(Optional.ofNullable(round.movePlayer2)
					.map(moveConverter::convert)
					.orElse(null));
		}

		public void convert(GameSessionEntity.RoundEmbedded roundEmbedded, DTOs.Round round) {
			round.movePlayer1 = Optional.ofNullable(roundEmbedded.getMovePlayer1())
					.map(moveConverter::convert)
					.orElse(null);

			round.movePlayer2 = Optional.ofNullable(roundEmbedded.getMovePlayer2())
					.map(moveConverter::convert)
					.orElse(null);
		}
	}
}
