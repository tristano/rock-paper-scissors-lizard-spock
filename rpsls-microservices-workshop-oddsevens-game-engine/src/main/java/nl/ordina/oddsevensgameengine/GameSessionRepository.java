package nl.ordina.oddsevensgameengine;

import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class GameSessionRepository implements PanacheMongoRepository<GameSessionEntity> {
    public Optional<GameSessionEntity> findBySessionUUID(String uuid) {
        return Optional.ofNullable(find("sessionUUID", uuid).firstResult());
    }

    public Optional<GameSessionEntity> findNewSession() {
        return Optional.ofNullable(find("status", GameSessionEntity.SessionStatusEmbedded.NEW).firstResult());
    }

    public GameSessionEntity makeNewSessionWithPlayerName(String playerName, String clientUUID) {
        GameSessionEntity.PlayerEmbedded player = new GameSessionEntity.PlayerEmbedded();
        player.setName(playerName);
        player.setClientUUID(clientUUID);
        player.setScore(0);
        GameSessionEntity gameSession = new GameSessionEntity();
        gameSession.setSessionUUID(UUID.randomUUID().toString());
        gameSession.setPlayer1(player);
        gameSession.setStatus(GameSessionEntity.SessionStatusEmbedded.NEW);
        persist(gameSession);
        return gameSession;
    }

    public GameSessionEntity updateNewSessionWithPlayerName(GameSessionEntity gameSessionEntity, String playerName, String clientUUID) {
        GameSessionEntity.PlayerEmbedded player = new GameSessionEntity.PlayerEmbedded();
        player.setName(playerName);
        player.setClientUUID(clientUUID);
        player.setScore(0);
        gameSessionEntity.setPlayer2(player);
        gameSessionEntity.setStatus(GameSessionEntity.SessionStatusEmbedded.NEW_ROUND);
        update(gameSessionEntity);
        return gameSessionEntity;
    }
}
