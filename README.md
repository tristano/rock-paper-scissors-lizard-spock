# Rock Paper Scissors Lizard Spock

### Import in Intellij IDEA
* Open project in Intellij IDEA
* Select directory
* For each java project
  - select pom.xml file
  - add as maven project

--- 

### Check the documentation online
The documentation is uploaded [here](https://rpsls-workshop-docs.netlify.com).
