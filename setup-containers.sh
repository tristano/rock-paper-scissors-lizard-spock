docker run -d --net=workshop.kafka-network --name=zookeeper1 -e ZOOKEEPER_CLIENT_PORT=2181 confluentinc/cp-zookeeper:5.3.1
docker run -d --net=workshop.kafka-network --name=kafka1 -p 9092:9092 -e KAFKA_ZOOKEEPER_CONNECT=zookeeper1:2181 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:9092 -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 confluentinc/cp-kafka:5.3.1
docker run -d --name mongo1 -p 27017:27017 mongo
docker run -d --name mongo2 -p 27018:27017 mongo
docker run -d --name rpsls-frontend-1 -p 80:80 tristano/rpsls-frontend:local
