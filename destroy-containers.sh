docker container stop zookeeper1
docker container stop kafka1
docker container stop mongo1
docker container stop mongo2

echo "Waiting 10 seconds..."
sleep 10

docker container rm --force zookeeper1
docker container rm --force kafka1
docker container rm --force mongo1
docker container rm --force mongo2
