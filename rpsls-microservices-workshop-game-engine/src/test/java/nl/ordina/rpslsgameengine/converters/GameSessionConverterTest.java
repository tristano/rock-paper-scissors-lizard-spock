package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.GameSessionEntity;
import nl.ordina.rpslsgameengine.cache.entities.embedded.PlayerEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.ResultEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.RoundEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Player;
import nl.ordina.rpslsgameengine.datatransfer.Result;
import nl.ordina.rpslsgameengine.datatransfer.Round;
import nl.ordina.rpslsgameengine.datatransfer.GameSession;
import nl.ordina.rpslsgameengine.datatransfer.GameSessionStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameSessionConverterTest {

    private GameSessionConverter converter;

    @BeforeEach
    void setUp() {
        converter = new GameSessionConverter();
        RoundConverter roundConverter = new RoundConverter();
        roundConverter.moveConverter = new MoveConverter();
        converter.playerConverter = new PlayerConverter();
        converter.roundConverter = roundConverter;
        converter.resultConverter = new ResultConverter();
        converter.gameSessionStatusConverter = new GameSessionStatusConverter();
    }

    @Test
    @DisplayName("Converts from GameSession to GameSessionEntity")
    void testConvertGameSessionToGameSessionEntity() {
        GameSession gameSession = new GameSession();
        gameSession.sessionUUID = "uuid";
        gameSession.player1 = new Player();
        gameSession.player2 = new Player();
        gameSession.status = GameSessionStatus.CANCELLED;
        gameSession.roundInProgress = new Round();
        gameSession.lastRound = new Round();
        gameSession.resultLastRound = Result.PLAYER_1_WINS;

        GameSessionEntity gameSessionEntity = converter.convert(gameSession);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals("uuid", gameSessionEntity.getSessionUUID()),
                () -> assertEquals(SessionStatusEmbedded.CANCELLED, gameSessionEntity.getStatus()),
                () -> assertEquals(ResultEmbedded.PLAYER_1_WINS, gameSessionEntity.getResultLastRound()));
    }

    @Test
    @DisplayName("Converts from GameSession to GameSessionEntity")
    void testConvertGameSessionEntityToGameSession() {
        GameSessionEntity gameSessionEntity = new GameSessionEntity();
        gameSessionEntity.setSessionUUID("uuid");
        gameSessionEntity.setPlayer1(new PlayerEmbedded());
        gameSessionEntity.setPlayer2(new PlayerEmbedded());
        gameSessionEntity.setStatus(SessionStatusEmbedded.CANCELLED);
        gameSessionEntity.setRoundInProgress(new RoundEmbedded());
        gameSessionEntity.setLastRound(new RoundEmbedded());
        gameSessionEntity.setResultLastRound(ResultEmbedded.PLAYER_1_WINS);

        GameSession gameSession = converter.convert(gameSessionEntity);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals("uuid", gameSession.sessionUUID),
                () -> assertEquals(GameSessionStatus.CANCELLED, gameSession.status),
                () -> assertEquals(Result.PLAYER_1_WINS, gameSession.resultLastRound));
    }

    @Test
    @DisplayName("Converts empty objects without generating NullPointerExceptions")
    void testConvertEmptyObjects() {
        converter.convert(new GameSession());
        converter.convert(new GameSessionEntity());
    }
}
