package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.ResultEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Result;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResultConverterTest {
    private ResultConverter converter;
    private static final int NUMBER_OF_RESULTS = 3;

    @BeforeEach
    void setup() {
        converter = new ResultConverter();
    }

    @Test
    @DisplayName("Converts from Result to ResultEmbedded")
    void testConvertResultToResultEmbedded() {
        Assertions.assertAll("It should return the right results",
                () -> assertEquals(ResultEmbedded.PLAYER_1_WINS, converter.convert(Result.PLAYER_1_WINS)),
                () -> assertEquals(ResultEmbedded.PLAYER_2_WINS, converter.convert(Result.PLAYER_2_WINS)),
                () -> assertEquals(ResultEmbedded.DRAW, converter.convert(Result.DRAW)));
    }

    @Test
    @DisplayName("Converts from ResultEmbedded to Result")
    void testConvertResultEmbeddedToResult() {
        Assertions.assertAll("It should return the right results",
                () -> assertEquals(Result.PLAYER_1_WINS, converter.convert(ResultEmbedded.PLAYER_1_WINS)),
                () -> assertEquals(Result.PLAYER_2_WINS, converter.convert(ResultEmbedded.PLAYER_2_WINS)),
                () -> assertEquals(Result.DRAW, converter.convert(ResultEmbedded.DRAW)));
    }

    @Test
    @DisplayName("Amount of different statuses is known and unchanged")
    void testAmountDifferentStatusesKnownAndUnchanged() {
        Assertions.assertAll("Amount of different results is known and unchanged",
                () -> assertEquals(NUMBER_OF_RESULTS, ResultEmbedded.values().length),
                () -> assertEquals(NUMBER_OF_RESULTS, Result.values().length));
    }
}
