package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.MoveEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MoveConverterTest {
    private MoveConverter converter;
    private static final int NUMBER_OF_MOVES = 5;

    @BeforeEach
    void setup() {
        converter = new MoveConverter();
    }

    @Test
    @DisplayName("Converts from Move to MoveEmbedded")
    void testConvertMoveToMoveEmbedded() {
        Assertions.assertAll("It should return the right results",
                () -> assertEquals(MoveEmbedded.ROCK, converter.convert(Move.ROCK)),
                () -> assertEquals(MoveEmbedded.PAPER, converter.convert(Move.PAPER)),
                () -> assertEquals(MoveEmbedded.SCISSORS, converter.convert(Move.SCISSORS)),
                () -> assertEquals(MoveEmbedded.LIZARD, converter.convert(Move.LIZARD)),
                () -> assertEquals(MoveEmbedded.SPOCK, converter.convert(Move.SPOCK)));
    }

    @Test
    @DisplayName("Converts from MoveEmbedded to Move")
    void testConvertMoveEmbeddedToMove() {
        Assertions.assertAll("It should return the right results",
                () -> assertEquals(Move.ROCK, converter.convert(MoveEmbedded.ROCK)),
                () -> assertEquals(Move.PAPER, converter.convert(MoveEmbedded.PAPER)),
                () -> assertEquals(Move.SCISSORS, converter.convert(MoveEmbedded.SCISSORS)),
                () -> assertEquals(Move.LIZARD, converter.convert(MoveEmbedded.LIZARD)),
                () -> assertEquals(Move.SPOCK, converter.convert(MoveEmbedded.SPOCK)));
    }

    @Test
    @DisplayName("Amount of different statuses is known and unchanged")
    void testAmountDifferentStatusesKnownAndUnchanged() {
        Assertions.assertAll("Amount of different moves is known and unchanged",
                () -> assertEquals(NUMBER_OF_MOVES, MoveEmbedded.values().length),
                () -> assertEquals(NUMBER_OF_MOVES, Move.values().length));
    }
}
