package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.MoveEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.RoundEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Move;
import nl.ordina.rpslsgameengine.datatransfer.Round;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoundConverterTest {

    private RoundConverter converter;
    private MoveConverter moveConverter;

    @BeforeEach
    void setUp() {
        converter = new RoundConverter();
        moveConverter = new MoveConverter();
        converter.moveConverter = moveConverter;
    }

    @Test
    @DisplayName("Converts from Round to RoundEmbedded")
    void testConvertRoundToRoundEmbedded() {
        Round roundDTO = new Round();
        roundDTO.movePlayer1 = Move.ROCK;
        roundDTO.movePlayer2 = Move.SCISSORS;

        RoundEmbedded roundEmbedded = converter.convert(roundDTO);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals(MoveEmbedded.ROCK, roundEmbedded.getMovePlayer1()),
                () -> assertEquals(MoveEmbedded.SCISSORS, roundEmbedded.getMovePlayer2()));
    }

    @Test
    @DisplayName("Converts from RoundEmbbedded to Round")
    void testConvertRoundEmbeddedToRound() {
        RoundEmbedded roundEmbedded = new RoundEmbedded();
        roundEmbedded.setMovePlayer1(MoveEmbedded.ROCK);
        roundEmbedded.setMovePlayer2(MoveEmbedded.SCISSORS);

        Round round = converter.convert(roundEmbedded);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals(Move.ROCK, round.movePlayer1),
                () -> assertEquals(Move.SCISSORS, round.movePlayer2));
    }

    @Test
    @DisplayName("Converts empty objects without generating NullPointerExceptions")
    void testConvertEmptyObjects() {
        converter.convert(new Round());
        converter.convert(new RoundEmbedded());
    }
}
