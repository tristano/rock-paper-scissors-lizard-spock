package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.PlayerEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerConverterTest {

    private PlayerConverter converter;

    @BeforeEach
    void setUp() {
        converter = new PlayerConverter();
    }

    @Test
    @DisplayName("Converts from Player to PlayerEmbedded")
    void testConvertPlayerToPlayerEmbedded() {
        Player player = new Player();
        player.score = 10;
        player.name = "Jan";
        player.clientUUID = "aluelueluelue";

        PlayerEmbedded playerEmbedded = converter.convert(player);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals("Jan", playerEmbedded.getName()),
                () -> assertEquals(10, playerEmbedded.getScore()),
                () -> assertEquals("aluelueluelue", playerEmbedded.getUuid()));
    }

    @Test
    @DisplayName("Converts from PlayerEmbedded to Player")
    void testConvertPlayerEmbeddedToPlayer() {
        PlayerEmbedded playerEmbedded = new PlayerEmbedded();
        playerEmbedded.setScore(10);
        playerEmbedded.setName("Jan");
        playerEmbedded.setUuid("aluelueluelue");

        Player player = converter.convert(playerEmbedded);

        Assertions.assertAll("It should give the right results",
                () -> assertEquals("Jan", player.name),
                () -> assertEquals(10, player.score),
                () -> assertEquals("aluelueluelue", player.clientUUID));
    }

    @Test
    @DisplayName("Converts empty objects without generating NullPointerExceptions")
    void testConvertEmptyObjects() {
        converter.convert(new Player());
        converter.convert(new PlayerEmbedded());
    }
}
