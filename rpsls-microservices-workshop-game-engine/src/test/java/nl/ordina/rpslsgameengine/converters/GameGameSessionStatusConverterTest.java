package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.GameSessionStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameGameSessionStatusConverterTest {
    private GameSessionStatusConverter converter;
    private static final int NUMBER_OF_STATUSES = 6;

    @BeforeEach
    void setup() {
        converter = new GameSessionStatusConverter();
    }

    @Test
    @DisplayName("Converts from GameSessionStatus to GameSessionStatusEmbedded")
    void testConvertSessionStatusToSessionStatusEmbedded() {
        Assertions.assertAll("It should return the right results",
                () -> assertEquals(SessionStatusEmbedded.NEW, converter.convert(GameSessionStatus.NEW)),
                () -> assertEquals(SessionStatusEmbedded.NEW_ROUND, converter.convert(GameSessionStatus.NEW_ROUND)),
                () -> assertEquals(SessionStatusEmbedded.TERMINATED, converter.convert(GameSessionStatus.TERMINATED)),
                () -> assertEquals(SessionStatusEmbedded.CANCELLED, converter.convert(GameSessionStatus.CANCELLED)),
                () -> assertEquals(SessionStatusEmbedded.WAITING_FOR_PLAYER1_CHOICE, converter.convert(GameSessionStatus.WAITING_FOR_PLAYER1_CHOICE)),
                () -> assertEquals(SessionStatusEmbedded.WAITING_FOR_PLAYER2_CHOICE, converter.convert(GameSessionStatus.WAITING_FOR_PLAYER2_CHOICE)));
    }

    @Test
    @DisplayName("Converts from GameSessionStatusEmbedded to GameSessionStatus")
    void testConvertSessionStatusEmbeddedToSessionStatus() {
        Assertions.assertAll("It should return the right results",
                () -> assertEquals(GameSessionStatus.NEW, converter.convert(SessionStatusEmbedded.NEW)),
                () -> assertEquals(GameSessionStatus.NEW_ROUND, converter.convert(SessionStatusEmbedded.NEW_ROUND)),
                () -> assertEquals(GameSessionStatus.TERMINATED, converter.convert(SessionStatusEmbedded.TERMINATED)),
                () -> assertEquals(GameSessionStatus.CANCELLED, converter.convert(SessionStatusEmbedded.CANCELLED)),
                () -> assertEquals(GameSessionStatus.WAITING_FOR_PLAYER1_CHOICE, converter.convert(SessionStatusEmbedded.WAITING_FOR_PLAYER1_CHOICE)),
                () -> assertEquals(GameSessionStatus.WAITING_FOR_PLAYER2_CHOICE, converter.convert(SessionStatusEmbedded.WAITING_FOR_PLAYER2_CHOICE)));
    }

    @Test
    @DisplayName("Amount of different statuses is known and unchanged")
    void testAmountDifferentStatusesKnownAndUnchanged() {
        Assertions.assertAll("Amount of different statuses is known and unchanged",
                () -> assertEquals(NUMBER_OF_STATUSES, SessionStatusEmbedded.values().length),
                () -> assertEquals(NUMBER_OF_STATUSES, GameSessionStatus.values().length));
    }
}
