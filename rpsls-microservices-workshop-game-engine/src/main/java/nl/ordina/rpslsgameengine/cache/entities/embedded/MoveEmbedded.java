package nl.ordina.rpslsgameengine.cache.entities.embedded;

public enum MoveEmbedded {
    ROCK, PAPER, SCISSORS, LIZARD, SPOCK
}
