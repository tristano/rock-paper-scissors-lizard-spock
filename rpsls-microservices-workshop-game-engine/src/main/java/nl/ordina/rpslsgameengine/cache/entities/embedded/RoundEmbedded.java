package nl.ordina.rpslsgameengine.cache.entities.embedded;

public class RoundEmbedded {
    private MoveEmbedded movePlayer1;
    private MoveEmbedded movePlayer2;

    public MoveEmbedded getMovePlayer1() {
        return movePlayer1;
    }

    public void setMovePlayer1(MoveEmbedded movePlayer1) {
        this.movePlayer1 = movePlayer1;
    }

    public MoveEmbedded getMovePlayer2() {
        return movePlayer2;
    }

    public void setMovePlayer2(MoveEmbedded movePlayer2) {
        this.movePlayer2 = movePlayer2;
    }
}
