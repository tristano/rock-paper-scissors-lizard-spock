package nl.ordina.rpslsgameengine.cache.entities.embedded;

public enum SessionStatusEmbedded {
    NEW, NEW_ROUND, CANCELLED, TERMINATED, WAITING_FOR_PLAYER1_CHOICE, WAITING_FOR_PLAYER2_CHOICE
}
