package nl.ordina.rpslsgameengine.cache.entities;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import nl.ordina.rpslsgameengine.cache.entities.embedded.PlayerEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.ResultEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.RoundEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded;

@MongoEntity(collection = "rpsls.sessions")
public class GameSessionEntity extends PanacheMongoEntity {
    private String sessionUUID;
    private PlayerEmbedded player1;
    private PlayerEmbedded player2;
    private SessionStatusEmbedded status;
    private RoundEmbedded roundInProgress;
    private RoundEmbedded lastRound;
    private ResultEmbedded resultLastRound;

    public String getSessionUUID() {
        return sessionUUID;
    }

    public void setSessionUUID(String sessionUUID) {
        this.sessionUUID = sessionUUID;
    }

    public PlayerEmbedded getPlayer1() {
        return player1;
    }

    public void setPlayer1(PlayerEmbedded player1) {
        this.player1 = player1;
    }

    public PlayerEmbedded getPlayer2() {
        return player2;
    }

    public void setPlayer2(PlayerEmbedded player2) {
        this.player2 = player2;
    }

    public SessionStatusEmbedded getStatus() {
        return status;
    }

    public void setStatus(SessionStatusEmbedded status) {
        this.status = status;
    }

    public RoundEmbedded getRoundInProgress() {
        return roundInProgress;
    }

    public void setRoundInProgress(RoundEmbedded roundInProgress) {
        this.roundInProgress = roundInProgress;
    }

    public ResultEmbedded getResultLastRound() {
        return resultLastRound;
    }

    public void setResultLastRound(ResultEmbedded resultLastRound) {
        this.resultLastRound = resultLastRound;
    }

    public RoundEmbedded getLastRound() {
        return lastRound;
    }

    public void setLastRound(RoundEmbedded lastRound) {
        this.lastRound = lastRound;
    }
}
