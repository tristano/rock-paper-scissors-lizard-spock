package nl.ordina.rpslsgameengine.cache.entities.embedded;

public class PlayerEmbedded {
    private String name;
    private String uuid;
    private int score;

    public void incrementScore() {
        score += 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
