package nl.ordina.rpslsgameengine.cache.repositories;

import io.quarkus.mongodb.panache.PanacheMongoRepository;
import nl.ordina.rpslsgameengine.cache.entities.GameSessionEntity;
import nl.ordina.rpslsgameengine.cache.entities.embedded.PlayerEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class GameSessionRepository implements PanacheMongoRepository<GameSessionEntity> {
    public Optional<GameSessionEntity> findBySessionUUID(String uuid) {
        return Optional.ofNullable(find("sessionUUID", uuid).firstResult());
    }

    public Optional<GameSessionEntity> findNewSession() {
        return Optional.ofNullable(find("status", SessionStatusEmbedded.NEW).firstResult());
    }

    public GameSessionEntity makeNewSessionWithPlayerName(String playerName, String clientUUID) {
        PlayerEmbedded player = new PlayerEmbedded();
        player.setName(playerName);
        player.setUuid(clientUUID);
        player.setScore(0);
        GameSessionEntity gameSession = new GameSessionEntity();
        gameSession.setSessionUUID(UUID.randomUUID().toString());
        gameSession.setPlayer1(player);
        gameSession.setStatus(SessionStatusEmbedded.NEW);
        persist(gameSession);
        return gameSession;
    }

    public GameSessionEntity updateNewSessionWithPlayerName(GameSessionEntity gameSessionEntity, String playerName, String clientUUID) {
        PlayerEmbedded player = new PlayerEmbedded();
        player.setName(playerName);
        player.setUuid(clientUUID);
        player.setScore(0);
        gameSessionEntity.setPlayer2(player);
        gameSessionEntity.setStatus(SessionStatusEmbedded.NEW_ROUND);
        update(gameSessionEntity);
        return gameSessionEntity;
    }
}
