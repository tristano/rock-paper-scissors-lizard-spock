package nl.ordina.rpslsgameengine.cache.entities.embedded;

public enum ResultEmbedded {
    PLAYER_1_WINS, PLAYER_2_WINS, DRAW
}
