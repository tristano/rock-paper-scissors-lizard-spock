package nl.ordina.rpslsgameengine.datatransfer;

public enum Result {
    PLAYER_1_WINS, PLAYER_2_WINS, DRAW
}
