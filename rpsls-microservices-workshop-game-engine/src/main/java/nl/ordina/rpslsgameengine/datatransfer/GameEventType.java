package nl.ordina.rpslsgameengine.datatransfer;

public enum GameEventType {
    NEW_ROUND, SESSION_CANCELLED, SESSION_TERMINATED
}
