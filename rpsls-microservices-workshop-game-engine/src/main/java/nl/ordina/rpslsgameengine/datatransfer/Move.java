package nl.ordina.rpslsgameengine.datatransfer;

public enum Move {
    ROCK, PAPER, SCISSORS, LIZARD, SPOCK
}
