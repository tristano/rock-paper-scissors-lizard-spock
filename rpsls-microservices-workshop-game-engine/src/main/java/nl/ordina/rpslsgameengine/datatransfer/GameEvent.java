package nl.ordina.rpslsgameengine.datatransfer;

import java.util.ArrayList;
import java.util.List;

public class GameEvent {
    public String sessionUUID;
    public GameEventType gameEventType;
    public Score score;
    public List<Player> recipients = new ArrayList<>();
}
