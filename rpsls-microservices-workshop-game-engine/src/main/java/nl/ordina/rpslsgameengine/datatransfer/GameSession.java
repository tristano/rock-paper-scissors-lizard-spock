package nl.ordina.rpslsgameengine.datatransfer;

public class GameSession {
    public String sessionUUID;
    public Player player1;
    public Player player2;
    public GameSessionStatus status;
    public Round roundInProgress;
    public Round lastRound;
    public Result resultLastRound;
}
