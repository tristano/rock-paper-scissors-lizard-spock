package nl.ordina.rpslsgameengine.messaging;

import io.smallrye.reactive.messaging.annotations.Broadcast;
import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;
import nl.ordina.rpslsgameengine.datatransfer.GameEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GameEventsProducer {
    @Inject
    @Broadcast
    @Channel(value = "rpsls-game-events")
    Emitter<GameEvent> eventEmitter;

    public void sendEvent(GameEvent gameEvent) {
        eventEmitter.send(gameEvent);
    }
}
