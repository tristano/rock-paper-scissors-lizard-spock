package nl.ordina.rpslsgameengine.messaging;

import io.smallrye.reactive.messaging.annotations.Broadcast;
import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;
import nl.ordina.rpslsgameengine.datatransfer.FinalResult;
import nl.ordina.rpslsgameengine.datatransfer.GameEvent;
import nl.ordina.rpslsgameengine.datatransfer.Score;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DashboardResultsProducer {
    @Inject
    @Broadcast
    @Channel(value = "dashboard-results")
    Emitter<FinalResult> eventEmitter;

    public void sendEvent(FinalResult finalResult) {
        eventEmitter.send(finalResult);
    }
}
