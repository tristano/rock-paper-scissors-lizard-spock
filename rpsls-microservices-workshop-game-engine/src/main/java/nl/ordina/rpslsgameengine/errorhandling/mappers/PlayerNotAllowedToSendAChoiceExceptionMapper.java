package nl.ordina.rpslsgameengine.errorhandling.mappers;

import nl.ordina.rpslsgameengine.errorhandling.exceptions.PlayerNotAllowedToSendAChoiceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PlayerNotAllowedToSendAChoiceExceptionMapper implements ExceptionMapper<PlayerNotAllowedToSendAChoiceException> {

    @Override
    public Response toResponse(PlayerNotAllowedToSendAChoiceException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .build();
    }
}

