package nl.ordina.rpslsgameengine.errorhandling.mappers;

import nl.ordina.rpslsgameengine.errorhandling.exceptions.NoSuchGameSessionException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NoSuchGameSessionExceptionMapper implements ExceptionMapper<NoSuchGameSessionException> {

    @Override
    public Response toResponse(NoSuchGameSessionException exception) {
        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }
}
