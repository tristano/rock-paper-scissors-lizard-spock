package nl.ordina.rpslsgameengine.errorhandling.mappers;

import nl.ordina.rpslsgameengine.errorhandling.exceptions.SessionIdsDoNotMatchException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SessionIdsDoNotMatchExceptionMapper implements ExceptionMapper<SessionIdsDoNotMatchException> {

    @Override
    public Response toResponse(SessionIdsDoNotMatchException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .build();
    }
}

