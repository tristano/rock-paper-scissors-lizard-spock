package nl.ordina.rpslsgameengine.errorhandling.mappers;

import nl.ordina.rpslsgameengine.errorhandling.exceptions.PlayerNotInGameSessionException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PlayerNotInGameSessionExceptionMapper implements ExceptionMapper<PlayerNotInGameSessionException> {

    @Override
    public Response toResponse(PlayerNotInGameSessionException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .build();
    }
}

