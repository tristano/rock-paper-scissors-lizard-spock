package nl.ordina.rpslsgameengine.services;

import nl.ordina.rpslsgameengine.cache.entities.GameSessionEntity;
import nl.ordina.rpslsgameengine.cache.entities.embedded.MoveEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.PlayerEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.ResultEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.RoundEmbedded;
import nl.ordina.rpslsgameengine.cache.repositories.GameSessionRepository;
import nl.ordina.rpslsgameengine.datatransfer.Choice;
import nl.ordina.rpslsgameengine.errorhandling.exceptions.NoSuchGameSessionException;
import nl.ordina.rpslsgameengine.errorhandling.exceptions.PlayerNotAllowedToSendAChoiceException;
import nl.ordina.rpslsgameengine.errorhandling.exceptions.PlayerNotInGameSessionException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.CANCELLED;
import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.NEW_ROUND;
import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.TERMINATED;
import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.WAITING_FOR_PLAYER1_CHOICE;
import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.WAITING_FOR_PLAYER2_CHOICE;

@ApplicationScoped
public class GameSessionService {

    @Inject
    GameSessionRepository gameSessionRepository;

    @Inject
    RuleService ruleService;

    private final static int SCORE_TO_WIN_THE_GAME_SESSION = 3;

    public List<GameSessionEntity> getAllSessions(String clientUUID, String username) {
        return gameSessionRepository.findAll().list();
    }

    public GameSessionEntity findSessionByUuid(String uuid) {
        return gameSessionRepository.findBySessionUUID(uuid).
                orElseThrow(NoSuchGameSessionException::new);
    }

    public GameSessionEntity registerPlayerToSession(String playerName, String clientUUID) {
        Optional<GameSessionEntity> session = gameSessionRepository.findNewSession();
        return session.map(value -> gameSessionRepository.updateNewSessionWithPlayerName(value, playerName, clientUUID))
                .orElseGet(() -> gameSessionRepository.makeNewSessionWithPlayerName(playerName, clientUUID));
    }

    public GameSessionEntity processChoiceAndUpdateGameSession(Choice choice) {
        GameSessionEntity gameSessionEntity = gameSessionRepository.findBySessionUUID(choice.sessionUUID)
                .orElseThrow(NoSuchGameSessionException::new);

        PlayerEmbedded player = getPlayerFromSession(gameSessionEntity, choice.clientUUID)
                .orElseThrow(PlayerNotInGameSessionException::new);

        checkIfPlayerIsAllowedToSendHisChoice(gameSessionEntity, player);

        RoundEmbedded round = getOrCreateRound(gameSessionEntity);

        MoveEmbedded move = MoveEmbedded.valueOf(choice.move.toString());

        if (isPlayer1(player, gameSessionEntity)) {
            round.setMovePlayer1(move);
            if (!isRoundCompleted(round)) {
                gameSessionEntity.setStatus(WAITING_FOR_PLAYER2_CHOICE);
            }
        }

        if (isPlayer2(player, gameSessionEntity)) {
            round.setMovePlayer2(move);
            if (!isRoundCompleted(round)) {
                gameSessionEntity.setStatus(WAITING_FOR_PLAYER1_CHOICE);
            }
        }

        if (isRoundCompleted(round)) {
            evaluateRoundAndUpdateScore(gameSessionEntity, round);
        } else {
            gameSessionEntity.update();
            return gameSessionEntity;
        }

        if (isGameSessionCompleted(gameSessionEntity)) {
            gameSessionEntity.setStatus(TERMINATED);
        } else {
            gameSessionEntity.setStatus(NEW_ROUND);
        }
        gameSessionEntity.update();
        return gameSessionEntity;
    }

    public void deleteAllSessions() {
        gameSessionRepository.deleteAll();
    }

    public void setSessionAsCancelled(GameSessionEntity gameSessionEntity) {
        gameSessionEntity.setStatus(CANCELLED);
        gameSessionRepository.update(gameSessionEntity);
    }

    private void evaluateRoundAndUpdateScore(GameSessionEntity gameSessionEntity, RoundEmbedded round) {
        ResultEmbedded result = ruleService.getRoundWinner(round.getMovePlayer1(), round.getMovePlayer2());
        gameSessionEntity.setResultLastRound(result);
        if (result == ResultEmbedded.PLAYER_1_WINS) {
            gameSessionEntity.getPlayer1().incrementScore();
        }
        if (result == ResultEmbedded.PLAYER_2_WINS) {
            gameSessionEntity.getPlayer2().incrementScore();
        }
        gameSessionEntity.setLastRound(round);
        gameSessionEntity.setRoundInProgress(null);
    }

    private RoundEmbedded getOrCreateRound(GameSessionEntity gameSessionEntity) {
        RoundEmbedded round = gameSessionEntity.getRoundInProgress();
        if (round == null) {
            round = new RoundEmbedded();
            gameSessionEntity.setRoundInProgress(round);
        }
        return round;
    }

    private boolean isPlayer1(PlayerEmbedded player, GameSessionEntity gameSessionEntity) {
        return player.getUuid().equals(gameSessionEntity.getPlayer1().getUuid());
    }

    private boolean isPlayer2(PlayerEmbedded player, GameSessionEntity gameSessionEntity) {
        return player.getUuid().equals(gameSessionEntity.getPlayer2().getUuid());
    }

    private boolean isRoundCompleted(RoundEmbedded round) {
        return round.getMovePlayer1() != null && round.getMovePlayer2() != null;
    }

    private boolean isGameSessionCompleted(GameSessionEntity gameSessionEntity) {
        return gameSessionEntity.getPlayer1().getScore() == SCORE_TO_WIN_THE_GAME_SESSION || gameSessionEntity.getPlayer2().getScore() == SCORE_TO_WIN_THE_GAME_SESSION;
    }

    private void checkIfPlayerIsAllowedToSendHisChoice(GameSessionEntity gameSessionEntity, PlayerEmbedded player) {
        if (!Arrays.asList(WAITING_FOR_PLAYER1_CHOICE, WAITING_FOR_PLAYER2_CHOICE, NEW_ROUND).contains(gameSessionEntity.getStatus())) {
            throw new PlayerNotAllowedToSendAChoiceException();
        }

        if (player == gameSessionEntity.getPlayer1() && gameSessionEntity.getStatus() == WAITING_FOR_PLAYER2_CHOICE) {
            throw new PlayerNotAllowedToSendAChoiceException();
        }

        if (player == gameSessionEntity.getPlayer2() && gameSessionEntity.getStatus() == WAITING_FOR_PLAYER1_CHOICE) {
            throw new PlayerNotAllowedToSendAChoiceException();
        }
    }

    private Optional<PlayerEmbedded> getPlayerFromSession(GameSessionEntity gameSessionEntity, String clientUUID) {
        return Stream.of(gameSessionEntity.getPlayer1(), gameSessionEntity.getPlayer2())
                .filter(player -> clientUUID.equals(player.getUuid()))
                .findFirst();
    }
}
