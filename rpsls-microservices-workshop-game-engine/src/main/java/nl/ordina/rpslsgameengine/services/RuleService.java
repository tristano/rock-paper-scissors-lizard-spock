package nl.ordina.rpslsgameengine.services;

import nl.ordina.rpslsgameengine.cache.entities.embedded.MoveEmbedded;
import nl.ordina.rpslsgameengine.cache.entities.embedded.ResultEmbedded;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class RuleService {
    private Map<MoveEmbedded, List<MoveEmbedded>> winningRules;

    public RuleService() {
        Map<MoveEmbedded, List<MoveEmbedded>> rules = new HashMap<>();
        rules.put(MoveEmbedded.ROCK, Arrays.asList(MoveEmbedded.SCISSORS, MoveEmbedded.LIZARD));
        rules.put(MoveEmbedded.PAPER, Arrays.asList(MoveEmbedded.ROCK, MoveEmbedded.SPOCK));
        rules.put(MoveEmbedded.SCISSORS, Arrays.asList(MoveEmbedded.PAPER, MoveEmbedded.LIZARD));
        rules.put(MoveEmbedded.LIZARD, Arrays.asList(MoveEmbedded.PAPER, MoveEmbedded.SPOCK));
        rules.put(MoveEmbedded.SPOCK, Arrays.asList(MoveEmbedded.SCISSORS, MoveEmbedded.ROCK));

        winningRules = Collections.unmodifiableMap(rules);
    }

    public ResultEmbedded getRoundWinner(MoveEmbedded movePlayer1, MoveEmbedded movePlayer2) {
        if (winningRules.get(movePlayer1).contains(movePlayer2)) {
            return ResultEmbedded.PLAYER_1_WINS;
        }

        if (winningRules.get(movePlayer2).contains(movePlayer1)) {
            return ResultEmbedded.PLAYER_2_WINS;
        }

        return ResultEmbedded.DRAW;
    }
}
