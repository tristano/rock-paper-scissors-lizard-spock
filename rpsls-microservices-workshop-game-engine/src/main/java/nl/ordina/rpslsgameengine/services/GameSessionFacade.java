package nl.ordina.rpslsgameengine.services;

import nl.ordina.rpslsgameengine.cache.entities.GameSessionEntity;
import nl.ordina.rpslsgameengine.cache.repositories.GameSessionRepository;
import nl.ordina.rpslsgameengine.converters.GameSessionConverter;
import nl.ordina.rpslsgameengine.datatransfer.Choice;
import nl.ordina.rpslsgameengine.datatransfer.FinalResult;
import nl.ordina.rpslsgameengine.datatransfer.GameEvent;
import nl.ordina.rpslsgameengine.datatransfer.GameEventType;
import nl.ordina.rpslsgameengine.datatransfer.Player;
import nl.ordina.rpslsgameengine.datatransfer.PlayerWithMove;
import nl.ordina.rpslsgameengine.datatransfer.Score;
import nl.ordina.rpslsgameengine.datatransfer.GameSession;
import nl.ordina.rpslsgameengine.errorhandling.exceptions.SessionIdsDoNotMatchException;
import nl.ordina.rpslsgameengine.messaging.DashboardResultsProducer;
import nl.ordina.rpslsgameengine.messaging.GameEventsProducer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.CANCELLED;
import static nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded.TERMINATED;

@ApplicationScoped
public class GameSessionFacade {
    @Inject
	GameSessionConverter gameSessionConverter;

    @Inject
	GameSessionService gameSessionService;

    @Inject
	GameSessionRepository gameSessionRepository;

    @Inject
	GameEventsProducer gameEventsProducer;

    @Inject
	DashboardResultsProducer dashboardResultsProducer;

    public List<GameSession> getAll(String clientUUID, String username) {
    	if(clientUUID != null && username != null) {
			return gameSessionService.getAllSessions(clientUUID, username).stream()
					.filter(session -> session.getStatus() != TERMINATED)
					.filter(session -> session.getStatus() != CANCELLED)
					.filter(session -> session.getPlayer1().getName().equals(username) ||
							(session.getPlayer2() != null && session.getPlayer2().getName().equals(username)))
					.filter(session -> session.getPlayer1().getUuid().equals(clientUUID) ||
							session.getPlayer2().getUuid().equals(clientUUID))
					.map(gameSessionConverter::convert)
					.collect(Collectors.toList());
		} else {
			return gameSessionService.getAllSessions(clientUUID, username).stream()
					.map(gameSessionConverter::convert)
					.collect(Collectors.toList());
		}
    }

    public void deleteAllSessions() {
        gameSessionService.deleteAllSessions();
    }

    public GameSession findSessionByUuid(String uuid) {
        return gameSessionConverter.convert(gameSessionService.findSessionByUuid(uuid));
    }

    public GameSession registerPlayerToSession(Player player) {
        GameSession gameSession = gameSessionConverter.convert(gameSessionService.registerPlayerToSession(player.name, player.clientUUID));
        if (gameSession.player2 != null && gameSession.player2.clientUUID.equals(player.clientUUID)) {
        	GameEvent gameEvent = new GameEvent();
        	gameEvent.gameEventType = GameEventType.NEW_ROUND;
        	gameEvent.sessionUUID = gameSession.sessionUUID;
			Score score = getScoreWithoutMoves(gameSession);
			gameEvent.score = score;
        	gameEvent.recipients.add(gameSession.player1);
        	gameEvent.recipients.add(gameSession.player2);
        	gameEventsProducer.sendEvent(gameEvent);
		}
        return gameSession;
    }

    public GameSession sendChoice(Choice choice, String sessionUUID) {
        if (!sessionUUID.equals(choice.sessionUUID)) {
            throw new SessionIdsDoNotMatchException();
        }

        GameSession gameSession = gameSessionConverter.convert(gameSessionService.processChoiceAndUpdateGameSession(choice));
        switch (gameSession.status) {
			case NEW_ROUND:
				GameEvent newRoundEvent = createEvent(gameSession, GameEventType.NEW_ROUND);
				newRoundEvent.recipients.add(newRoundEvent.score.player1);
				newRoundEvent.recipients.add(newRoundEvent.score.player2);
				gameEventsProducer.sendEvent(newRoundEvent);
				break;

			case TERMINATED:
				GameEvent sessionTerminatedEvent = createEvent(gameSession, GameEventType.SESSION_TERMINATED);
				sessionTerminatedEvent.recipients.add(gameSession.player1);
				sessionTerminatedEvent.recipients.add(gameSession.player2);
				gameEventsProducer.sendEvent(sessionTerminatedEvent);

				FinalResult finalResult = new FinalResult();
				finalResult.score = sessionTerminatedEvent.score;
				dashboardResultsProducer.sendEvent(finalResult);
				break;
		}

        return gameSession;
    }

	private GameEvent createEvent(GameSession gameSession, GameEventType gameEventType) {
		GameEvent gameEvent = new GameEvent();
		gameEvent.gameEventType = gameEventType;
		gameEvent.sessionUUID = gameSession.sessionUUID;
		if (gameSession.resultLastRound == null) {
			gameEvent.score = getScoreWithoutMoves(gameSession);
		} else {
			gameEvent.score = getScoreWithMoves(gameSession);
		}
		return gameEvent;
	}

	private PlayerWithMove getPlayer1WithMove(GameSession gameSession) {
		PlayerWithMove player1 = new PlayerWithMove();
		player1.clientUUID = gameSession.player1.clientUUID;
		player1.name = gameSession.player1.name;
		player1.score = gameSession.player1.score;
		player1.move = gameSession.lastRound.movePlayer1;
		return player1;
	}

	private PlayerWithMove getPlayer2WithMove(GameSession gameSession) {
		PlayerWithMove player2 = new PlayerWithMove();
		player2.clientUUID = gameSession.player2.clientUUID;
		player2.name = gameSession.player2.name;
		player2.score = gameSession.player2.score;
		player2.move = gameSession.lastRound.movePlayer2;
		return player2;
	}

	private Score getScoreWithMoves(GameSession gameSession) {
		PlayerWithMove player1 = getPlayer1WithMove(gameSession);
		PlayerWithMove player2 = getPlayer2WithMove(gameSession);

		Score score = new Score();
		score.player1 = player1;
		score.player2 = player2;

		return score;
	}

	private Score getScoreWithoutMoves(GameSession gameSession) {
		Score score = new Score();
		score.player1 = gameSession.player1;
		score.player2 = gameSession.player2;
		return score;
	}

	public void cancelSession(String sessionUUID) {
        gameSessionRepository.findBySessionUUID(sessionUUID)
				.filter(session -> session.getStatus() != TERMINATED)
                .ifPresent(session -> {
                	if (session.getStatus() != CANCELLED) {
						gameSessionService.setSessionAsCancelled(session);
						GameSession gameSession = gameSessionConverter.convert(session);
						GameEvent gameEvent = createEvent(gameSession, GameEventType.SESSION_CANCELLED);
						if (gameSession.player1 != null) {
							gameEvent.recipients.add(gameSession.player1);
						}
						if (gameSession.player2 != null) {
							gameEvent.recipients.add(gameSession.player2);
						}
						gameEventsProducer.sendEvent(gameEvent);
					}
				});
	}
}
