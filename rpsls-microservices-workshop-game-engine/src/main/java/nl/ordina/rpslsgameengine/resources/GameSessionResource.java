package nl.ordina.rpslsgameengine.resources;

import nl.ordina.rpslsgameengine.datatransfer.Choice;
import nl.ordina.rpslsgameengine.datatransfer.Player;
import nl.ordina.rpslsgameengine.datatransfer.GameSession;
import nl.ordina.rpslsgameengine.services.GameSessionFacade;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/session")
public class GameSessionResource {
    @Inject
	GameSessionFacade gameSessionFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public List<GameSession> getAll(@QueryParam("clientuuid") String clientUUID,
                                    @QueryParam("username") String username) {
        return gameSessionFacade.getAll(clientUUID, username);
    }

    @DELETE
    @Path("/")
    public void delete() {
        gameSessionFacade.deleteAllSessions();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{uuid}")
    public GameSession get(@PathParam("uuid") String uuid) {
        return gameSessionFacade.findSessionByUuid(uuid);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GameSession registerPlayerToSession(Player player) {
        return gameSessionFacade.registerPlayerToSession(player);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{sessionUUID}")
    public GameSession sendChoice(Choice choice, @PathParam("sessionUUID") String sessionUUID) {
        return gameSessionFacade.sendChoice(choice, sessionUUID);
    }

    @DELETE
    @Path("/{sessionUUID}/status")
    public void changeStatus(@PathParam("sessionUUID") String sessionUUID) {
        gameSessionFacade.cancelSession(sessionUUID);
    }
}
