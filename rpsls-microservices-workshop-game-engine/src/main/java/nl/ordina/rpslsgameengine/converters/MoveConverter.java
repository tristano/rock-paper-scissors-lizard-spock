package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.MoveEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Move;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MoveConverter {
    public MoveEmbedded convert(Move move) {
        return MoveEmbedded.valueOf(move.toString());
    }

    public Move convert(MoveEmbedded moveEmbedded) {
        return Move.valueOf(moveEmbedded.toString());
    }
}
