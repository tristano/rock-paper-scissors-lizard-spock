package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.RoundEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Round;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;

@ApplicationScoped
public class RoundConverter {
    @Inject
    MoveConverter moveConverter;

    public RoundEmbedded convert(Round round) {
        RoundEmbedded roundEmbedded = new RoundEmbedded();
        convert(round, roundEmbedded);
        return roundEmbedded;
    }

    public Round convert(RoundEmbedded roundEmbedded) {
        Round round = new Round();
        convert(roundEmbedded, round);
        return round;
    }

    public void convert(Round round, RoundEmbedded roundEmbedded) {
        roundEmbedded.setMovePlayer1(Optional.ofNullable(round.movePlayer1)
                .map(moveConverter::convert)
                .orElse(null));

        roundEmbedded.setMovePlayer2(Optional.ofNullable(round.movePlayer2)
                .map(moveConverter::convert)
                .orElse(null));
    }

    public void convert(RoundEmbedded roundEmbedded, Round round) {
        round.movePlayer1 = Optional.ofNullable(roundEmbedded.getMovePlayer1())
                .map(moveConverter::convert)
                .orElse(null);

        round.movePlayer2 = Optional.ofNullable(roundEmbedded.getMovePlayer2())
                .map(moveConverter::convert)
                .orElse(null);
    }
}
