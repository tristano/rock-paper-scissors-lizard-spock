package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.ResultEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Result;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResultConverter {
    public ResultEmbedded convert(Result result) {
        return ResultEmbedded.valueOf(result.toString());
    }

    public Result convert(ResultEmbedded resultEmbedded) {
        return Result.valueOf(resultEmbedded.toString());
    }
}
