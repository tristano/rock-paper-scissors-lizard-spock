package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.SessionStatusEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.GameSessionStatus;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GameSessionStatusConverter {
    public SessionStatusEmbedded convert(GameSessionStatus gameSessionStatus) {
        return SessionStatusEmbedded.valueOf(gameSessionStatus.toString());
    }

    public GameSessionStatus convert(SessionStatusEmbedded sessionStatusEmbedded) {
        return GameSessionStatus.valueOf(sessionStatusEmbedded.toString());
    }
}
