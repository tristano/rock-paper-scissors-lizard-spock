package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.embedded.PlayerEmbedded;
import nl.ordina.rpslsgameengine.datatransfer.Player;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PlayerConverter {
    public PlayerEmbedded convert(Player player) {
        PlayerEmbedded playerEmbedded = new PlayerEmbedded();
        convert(player, playerEmbedded);
        return playerEmbedded;
    }

    public Player convert(PlayerEmbedded playerEmbedded) {
        Player player = new Player();
        convert(playerEmbedded, player);
        return player;
    }

    public void convert(Player player, PlayerEmbedded playerEmbedded) {
        playerEmbedded.setUuid(player.clientUUID);
        playerEmbedded.setName(player.name);
        playerEmbedded.setScore(player.score);
    }

    public void convert(PlayerEmbedded playerEmbedded, Player player) {
        player.clientUUID = playerEmbedded.getUuid();
        player.name = playerEmbedded.getName();
        player.score = playerEmbedded.getScore();
    }
}
