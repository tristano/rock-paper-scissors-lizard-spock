package nl.ordina.rpslsgameengine.converters;

import nl.ordina.rpslsgameengine.cache.entities.GameSessionEntity;
import nl.ordina.rpslsgameengine.datatransfer.GameSession;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;

@ApplicationScoped
public class GameSessionConverter {
    @Inject
    PlayerConverter playerConverter;

    @Inject
    RoundConverter roundConverter;

    @Inject
    ResultConverter resultConverter;

    @Inject
    GameSessionStatusConverter gameSessionStatusConverter;

    public GameSessionEntity convert(GameSession gameSession) {
        GameSessionEntity gameSessionEntity = new GameSessionEntity();
        convert(gameSession, gameSessionEntity);
        return gameSessionEntity;
    }

    public GameSession convert(GameSessionEntity gameSessionEntity) {
        GameSession gameSession = new GameSession();
        convert(gameSessionEntity, gameSession);
        return gameSession;
    }

    public void convert(GameSession gameSession, GameSessionEntity gameSessionEntity) {
        gameSessionEntity.setSessionUUID(gameSession.sessionUUID);
        gameSessionEntity.setPlayer1(Optional.ofNullable(gameSession.player1)
                .map(playerConverter::convert)
                .orElse(null));

        gameSessionEntity.setPlayer2(Optional.ofNullable(gameSession.player2)
                .map(playerConverter::convert)
                .orElse(null));

        gameSessionEntity.setRoundInProgress(Optional.ofNullable(gameSession.roundInProgress)
                .map(roundConverter::convert)
                .orElse(null));

        gameSessionEntity.setLastRound(Optional.ofNullable(gameSession.lastRound)
                .map(roundConverter::convert)
                .orElse(null));

        gameSessionEntity.setResultLastRound(Optional.ofNullable(gameSession.resultLastRound)
                .map(resultConverter::convert)
                .orElse(null));

        gameSessionEntity.setStatus(Optional.ofNullable(gameSession.status)
                .map(gameSessionStatusConverter::convert)
                .orElse(null));
    }

    public void convert(GameSessionEntity gameSessionEntity, GameSession gameSession) {
        gameSession.sessionUUID = gameSessionEntity.getSessionUUID();
        gameSession.player1 = Optional.ofNullable(gameSessionEntity.getPlayer1())
                .map(playerConverter::convert)
                .orElse(null);

        gameSession.player2 = Optional.ofNullable(gameSessionEntity.getPlayer2())
                .map(playerConverter::convert)
                .orElse(null);

        gameSession.roundInProgress = Optional.ofNullable(gameSessionEntity.getRoundInProgress())
                .map(roundConverter::convert)
                .orElse(null);

        gameSession.lastRound = Optional.ofNullable(gameSessionEntity.getLastRound())
                .map(roundConverter::convert)
                .orElse(null);

        gameSession.resultLastRound = Optional.ofNullable(gameSessionEntity.getResultLastRound())
                .map(resultConverter::convert)
                .orElse(null);

        gameSession.status = Optional.ofNullable(gameSessionEntity.getStatus())
                .map(gameSessionStatusConverter::convert)
                .orElse(null);
    }
}
