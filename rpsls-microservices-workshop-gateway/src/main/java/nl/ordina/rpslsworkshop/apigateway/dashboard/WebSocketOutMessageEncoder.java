package nl.ordina.rpslsworkshop.apigateway.dashboard;

import com.google.gson.Gson;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class WebSocketOutMessageEncoder implements Encoder.Text<DashboardDTOs.WebSocketOutMessage> {
    private static Gson gson = new Gson();

    @Override
    public String encode(DashboardDTOs.WebSocketOutMessage webSocketOutMessage) throws EncodeException {
        return gson.toJson(webSocketOutMessage);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        // no op
    }

    @Override
    public void destroy() {
        // no op
    }
}
