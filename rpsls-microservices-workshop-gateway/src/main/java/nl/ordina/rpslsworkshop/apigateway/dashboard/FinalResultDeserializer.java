package nl.ordina.rpslsworkshop.apigateway.dashboard;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;

public class FinalResultDeserializer extends JsonbDeserializer<DashboardDTOs.FinalResult> {
	public FinalResultDeserializer() {
		super(DashboardDTOs.FinalResult.class);
	}
}
