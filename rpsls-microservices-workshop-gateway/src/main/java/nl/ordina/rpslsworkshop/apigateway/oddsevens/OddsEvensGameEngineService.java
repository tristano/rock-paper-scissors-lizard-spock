package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/session")
@RegisterRestClient
public interface OddsEvensGameEngineService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	List<OddsEvensDTOs.GameSession> getAll(@QueryParam("clientuuid") String clientUUID,
										  @QueryParam("username") String username);
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	OddsEvensDTOs.GameSession registerPlayerToSession(OddsEvensDTOs.Player player);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{sessionUUID}")
	public OddsEvensDTOs.GameSession sendChoice(OddsEvensDTOs.Choice choice, @PathParam("sessionUUID") String sessionUUID);

	@DELETE
	@Path("/{sessionUUID}/status")
	public void cancelSession(@PathParam("sessionUUID") String sessionUUID);
}
