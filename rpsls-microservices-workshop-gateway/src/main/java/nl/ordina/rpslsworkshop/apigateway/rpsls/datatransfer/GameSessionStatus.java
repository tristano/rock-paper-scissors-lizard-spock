package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer;

public enum GameSessionStatus {
    NEW, NEW_ROUND, CANCELLED, TERMINATED, WAITING_FOR_PLAYER1_CHOICE, WAITING_FOR_PLAYER2_CHOICE
}
