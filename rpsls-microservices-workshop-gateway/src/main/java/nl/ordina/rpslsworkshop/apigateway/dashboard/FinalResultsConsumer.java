package nl.ordina.rpslsworkshop.apigateway.dashboard;

import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FinalResultsConsumer {
	@Inject
	DashboardWebSocketSessionsContainer webSocketSessionsContainer;

	@Incoming("dashboard-results-topic")
	public void receiveEvent(DashboardDTOs.FinalResult finalResult) {
		DashboardDTOs.WebSocketOutMessage webSocketOutMessage = new DashboardDTOs.WebSocketOutMessage();
		finalResult.score.player1.clientUUID = null;
		finalResult.score.player2.clientUUID = null;
		webSocketOutMessage.header = DashboardDTOs.WebSocketOutMessage.Header.RESULT_RECEIVED;
		webSocketOutMessage.payload = finalResult;
		webSocketSessionsContainer.broadcast(webSocketOutMessage);
	}
}
