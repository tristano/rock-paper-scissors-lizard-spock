package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.websockets;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Choice;

public class WebSocketInMessage {
    public Header header;
    public Choice payload;
    public String sessionUUID;

    public enum Header {
        CHOOSE
    }
}
