package nl.ordina.rpslsworkshop.apigateway.rpsls.services;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Choice;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Player;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.GameSession;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/session")
@RegisterRestClient
public interface GameEngineService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public List<GameSession> getAll(@QueryParam("clientuuid") String clientUUID,
									@QueryParam("username") String username);
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	GameSession registerPlayerToSession(Player player);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{sessionUUID}")
	GameSession sendChoice(Choice choice, @PathParam("sessionUUID") String sessionUUID);

	@DELETE
	@Path("/{sessionUUID}/status")
	void cancelSession(@PathParam("sessionUUID") String sessionUUID);
}

