package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import com.google.gson.Gson;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class WebSocketInMessageDecoder implements Decoder.Text<OddsEvensDTOs.WebSocketInMessage> {
    private static Gson gson = new Gson();

    @Override
    public OddsEvensDTOs.WebSocketInMessage decode(String json) throws DecodeException {
        return gson.fromJson(json, OddsEvensDTOs.WebSocketInMessage.class);
    }

    @Override
    public boolean willDecode(String json) {
        return true;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        //no op
    }

    @Override
    public void destroy() {
        //no op
    }
}
