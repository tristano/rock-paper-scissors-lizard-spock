package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects;

public enum Result {
    PLAYER_1_WINS, PLAYER_2_WINS, DRAW
}
