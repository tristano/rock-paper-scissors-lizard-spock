package nl.ordina.rpslsworkshop.apigateway.dashboard;

import nl.ordina.rpslsworkshop.apigateway.common.GameKey;

class DashboardDTOs {
	public static class Player {
		public String name;
		public String clientUUID;
		public int score;
	}

	public static class Score {
		public Player player1;
		public Player player2;
	}

	public static class FinalResult {
		public GameKey gameKey;
		public Score score;
	}

	public static class WebSocketOutMessage {
		public Header header;
		public FinalResult payload;

		public enum Header {
			RESULT_RECEIVED
		}
	}
}
