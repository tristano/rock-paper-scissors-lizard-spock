package nl.ordina.rpslsworkshop.apigateway.dashboard;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.Session;

@ServerEndpoint(value = "/socket/dashboard/results/{clientUUID}",
        encoders = {WebSocketOutMessageEncoder.class})
@ApplicationScoped
public class DashboardSocket {

    @Inject
    DashboardWebSocketSessionsContainer webSocketSessionsContainer;

    @OnOpen
    public void onOpen(Session session, @PathParam("clientUUID") String clientUUID) {
        webSocketSessionsContainer.addSession(clientUUID, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("clientUUID") String clientUUID) {
        webSocketSessionsContainer.removeSession(clientUUID);
    }

    @OnError
    public void onError(Session session, @PathParam("clientUUID") String clientUUID, Throwable throwable) {
        webSocketSessionsContainer.removeSession(clientUUID);
    }
}
