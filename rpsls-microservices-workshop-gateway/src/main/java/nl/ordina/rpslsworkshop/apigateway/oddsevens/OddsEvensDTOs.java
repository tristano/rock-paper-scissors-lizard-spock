package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import java.util.ArrayList;
import java.util.List;

class OddsEvensDTOs {
	public enum GameSessionStatus {
		NEW, NEW_ROUND, CANCELLED, TERMINATED, WAITING_FOR_PLAYER1_CHOICE, WAITING_FOR_PLAYER2_CHOICE
	}

	public enum Move {
		ONE, TWO, THREE, FOUR, FIVE
	}

	public enum Result {
		PLAYER_1_WINS, PLAYER_2_WINS
	}

	public static class Player {
		public String name;
		public String clientUUID;
		public int score;
	}

	public static class PlayerWithMove extends Player {
		public Move move;
	}

	public static class Round {
		public Move movePlayer1;
		public Move movePlayer2;
	}

	public static class Score {
		public PlayerWithMove player1;
		public PlayerWithMove player2;
	}

	public static class GameEvent {
		public String sessionUUID;
		public GameEventType gameEventType;
		public Score score;
		public List<Player> recipients = new ArrayList<>();
	}

	public enum GameEventType {
		NEW_ROUND, SESSION_CANCELLED, SESSION_TERMINATED
	}

	public static class GameSession {
		public String sessionUUID;
		public Player player1;
		public Player player2;
		public GameSessionStatus status;
		public Round roundInProgress;
		public Round lastRound;
		public Result resultLastRound;
	}

	public static class Choice {
		public String sessionUUID;
		public String clientUUID;
		public Move move;
	}

	public static class WebSocketInMessage {
		public Header header;
		public Choice payload;
		public String sessionUUID;

		public enum Header {
			CHOOSE
		}
	}

	public static class WebSocketOutMessage {
		public Header header;
		public Score payload;
		public String sessionUUID;

		public enum Header {
			NEW_ROUND, SESSION_TERMINATED, SESSION_CANCELLED
		}
	}
}
