package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects;

public enum Move {
	ROCK, PAPER, SCISSORS, LIZARD, SPOCK
}
