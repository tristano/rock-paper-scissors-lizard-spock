package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.serialization;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.GameEvent;


public class GameEventDeserializer extends JsonbDeserializer<GameEvent> {
	public GameEventDeserializer() {
		super(GameEvent.class);
	}
}
