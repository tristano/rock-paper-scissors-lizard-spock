package nl.ordina.rpslsworkshop.apigateway.common;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.websockets.WebSocketOutMessage;

import javax.websocket.Session;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public abstract class WebSocketSessionsContainer {
    private Map<SessionKey, Session> webSocketSessions =  new ConcurrentHashMap<>();

    public boolean isSessionInThisInstance(SessionKey sessionKey) {
    	return webSocketSessions.get(sessionKey) != null;
	}

    public void addSession(String clientUUID, String username, Session session) {
        webSocketSessions.put(makeSessionKey(clientUUID, username), session);
    }

    public void removeSession(String clientUUID, String username) {
        webSocketSessions.remove(makeSessionKey(clientUUID, username));
    }

    public List<SessionKeySession> getSessionPairs() {
        return webSocketSessions.entrySet().stream()
                .map(entry -> new SessionKeySession(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    public void send(SessionKey sessionKey, Object message) {
        send(sessionKey.getUuid(), sessionKey.getUsername(), message, () -> {});
    }

    public void send(String clientUUID, String username, Object message, Runnable callback) {
        getSessionPair(clientUUID, username)
                .map(SessionKeySession::getSession)
                .ifPresent(session -> send(session, message, callback));
    }

    private void send(Session session, Object message, Runnable callback) {
        session.getAsyncRemote().sendObject(message, result -> {
            if (result.isOK()) {
                callback.run();
            } else {
                System.out.println("Unable to send message: " + result.getException()); //TODO add logging instead of sout
            }
        });
    }

	private SessionKey makeSessionKey(String clientUUID, String username) {
		return new SessionKey(clientUUID, username);
	}

	private Optional<SessionKeySession> getSessionPair(String clientUUID, String username) {
		return getSessionPairs().stream()
				.filter(pair -> pair.getSessionKey().equals(makeSessionKey(clientUUID, username)))
				.findFirst();
	}
}
