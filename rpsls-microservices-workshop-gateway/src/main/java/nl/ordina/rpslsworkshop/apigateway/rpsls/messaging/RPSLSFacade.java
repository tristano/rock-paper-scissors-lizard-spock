package nl.ordina.rpslsworkshop.apigateway.rpsls.messaging;

import nl.ordina.rpslsworkshop.apigateway.common.SessionKey;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Choice;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.GameEvent;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Player;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.PlayerWithMove;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Score;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.websockets.WebSocketInMessage;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.websockets.WebSocketOutMessage;
import nl.ordina.rpslsworkshop.apigateway.rpsls.services.GameEngineService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.Session;
import java.util.Objects;

@ApplicationScoped
public class RPSLSFacade {

    @Inject
	RPSLSWebSocketSessionsContainer webSocketSessionsContainer;

    @Inject
    @RestClient
    GameEngineService gameEngineService;

    public void openNewSession(Session session, String clientUUID, String username) {
		webSocketSessionsContainer.addSession(clientUUID, username, session);
    	Player player = new Player();
    	player.name = username;
    	player.clientUUID = clientUUID;
		gameEngineService.registerPlayerToSession(player);
    }

    public void cleanUpTerminatedOrCancelledSession(String clientUUID, String username) {
        gameEngineService.getAll(clientUUID, username).stream().findFirst()
                .ifPresent(sessionDTO -> {
                    gameEngineService.cancelSession(sessionDTO.sessionUUID);
                });
        webSocketSessionsContainer.removeSession(clientUUID, username);
    }

    public void handleChooseMoveUserMessage(WebSocketInMessage message) {
		Choice choice = message.payload;
		gameEngineService.sendChoice(choice, choice.sessionUUID);
    }

    public void handleNewRoundGameEvent(GameEvent gameEvent) {
		handleGameEvent(gameEvent, WebSocketOutMessage.Header.NEW_ROUND);
	}

	public void handleSessionTerminatedGameEvent(GameEvent gameEvent) {
		handleGameEvent(gameEvent, WebSocketOutMessage.Header.SESSION_TERMINATED);
	}

	public void handleSessionCancelledGameEvent(GameEvent gameEvent) {
		handleGameEvent(gameEvent, WebSocketOutMessage.Header.SESSION_CANCELLED);
	}

	private void handleGameEvent(GameEvent gameEvent, WebSocketOutMessage.Header header) {
		gameEvent.recipients.stream()
				.filter(Objects::nonNull)
				.forEach(player -> {
					SessionKey sessionKey = new SessionKey(player.clientUUID, player.name);
					if (webSocketSessionsContainer.isSessionInThisInstance(sessionKey)) {
							WebSocketOutMessage webSocketOutMessage = new WebSocketOutMessage();
							Score payload = new Score();
							if (gameEvent.score.player1 != null) {
								PlayerWithMove player1 = new PlayerWithMove();
								player1.name = gameEvent.score.player1.name;
								player1.score = gameEvent.score.player1.score;
								player1.move = gameEvent.score.player1.move;
								if (player.clientUUID.equals(gameEvent.score.player1.clientUUID)) {
									player1.clientUUID = gameEvent.score.player1.clientUUID;
								}
								payload.player1 = player1;
							}
							if (gameEvent.score.player2 != null) {
								PlayerWithMove player2 = new PlayerWithMove();
								player2.name = gameEvent.score.player2.name;
								player2.score = gameEvent.score.player2.score;
								player2.move = gameEvent.score.player2.move;
								if (player.clientUUID.equals(gameEvent.score.player2.clientUUID)) {
									player2.clientUUID = gameEvent.score.player2.clientUUID;
								}
								payload.player2 = player2;
							}
							webSocketOutMessage.header = header;
							webSocketOutMessage.payload = payload;
							webSocketOutMessage.sessionUUID = gameEvent.sessionUUID;
							webSocketSessionsContainer.send(sessionKey, webSocketOutMessage);
					}
		});
	}
}
