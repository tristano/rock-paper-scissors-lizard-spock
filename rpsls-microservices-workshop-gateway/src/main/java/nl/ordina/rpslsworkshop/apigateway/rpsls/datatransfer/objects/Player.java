package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects;

public class Player {
	public String name;
	public String clientUUID;
	public Integer score;
}
