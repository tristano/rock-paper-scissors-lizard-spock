package nl.ordina.rpslsworkshop.apigateway.common;

import java.util.Objects;

public class SessionKey {
    private String uuid;
    private String username;

    public SessionKey(String uuid, String username) {
        this.uuid = uuid;
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionKey that = (SessionKey) o;
        return uuid.equals(that.uuid) &&
                username.equals(that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, username);
    }
}
