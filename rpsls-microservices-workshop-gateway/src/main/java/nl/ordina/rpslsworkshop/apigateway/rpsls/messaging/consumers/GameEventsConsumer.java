package nl.ordina.rpslsworkshop.apigateway.rpsls.messaging.consumers;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.GameEvent;
import nl.ordina.rpslsworkshop.apigateway.rpsls.messaging.RPSLSFacade;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GameEventsConsumer {

    @Inject
    RPSLSFacade rpslsFacade;

    @Incoming("rpsls-game-events-topic")
    public void receiveEvent(GameEvent gameEvent) {
        switch (gameEvent.gameEventType) {
            case NEW_ROUND:
                rpslsFacade.handleNewRoundGameEvent(gameEvent);
                break;

            case SESSION_TERMINATED:
                rpslsFacade.handleSessionTerminatedGameEvent(gameEvent);
                break;

            case SESSION_CANCELLED:
                rpslsFacade.handleSessionCancelledGameEvent(gameEvent);
                break;
        }
    }
}
