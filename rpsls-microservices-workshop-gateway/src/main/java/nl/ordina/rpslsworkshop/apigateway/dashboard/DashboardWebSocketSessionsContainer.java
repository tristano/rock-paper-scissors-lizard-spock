package nl.ordina.rpslsworkshop.apigateway.dashboard;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ApplicationScoped
public class DashboardWebSocketSessionsContainer {
	private Map<String, Session> sessions = new ConcurrentHashMap<>();

	public void addSession(String clientUUID, Session session) {
		sessions.put(clientUUID, session);
	}

	public void removeSession(String clientUUID) {
		sessions.remove(clientUUID);
	}

	public void broadcast(Object message) {
		sessions.values()
				.forEach(session -> send(session, message));
	}

	private void send(Session session, Object message) {
		session.getAsyncRemote().sendObject(message, result -> {
			if (!result.isOK()) {
				System.out.println("Unable to send message: " + result.getException()); //TODO add logging instead of sout
			}
		});
	}
}
