package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;

public class GameEventDeserializer extends JsonbDeserializer<OddsEvensDTOs.GameEvent> {
	public GameEventDeserializer() {
		super(OddsEvensDTOs.GameEvent.class);
	}
}
