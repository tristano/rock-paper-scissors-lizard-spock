package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects;

public enum GameEventType {
	NEW_ROUND, SESSION_CANCELLED, SESSION_TERMINATED
}
