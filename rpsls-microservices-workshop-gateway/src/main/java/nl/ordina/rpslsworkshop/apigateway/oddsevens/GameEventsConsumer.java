package nl.ordina.rpslsworkshop.apigateway.oddsevens;


import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GameEventsConsumer {

    @Inject
    OddsEvensFacade evenOddsFacade;

    @Incoming("oddsevens-game-events-topic")
    public void receiveEvent(OddsEvensDTOs.GameEvent gameEvent) {
        switch (gameEvent.gameEventType) {
            case NEW_ROUND:
                evenOddsFacade.handleNewRoundGameEvent(gameEvent);
                break;

            case SESSION_TERMINATED:
                evenOddsFacade.handleSessionTerminatedGameEvent(gameEvent);
                break;

            case SESSION_CANCELLED:
                evenOddsFacade.handleSessionCancelledGameEvent(gameEvent);
                break;
        }
    }
}
