package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects;

import java.util.List;

public class GameEvent {
	public String sessionUUID;
	public GameEventType gameEventType;
	public Score score;
	public List<PlayerWithMove> recipients;
}
