package nl.ordina.rpslsworkshop.apigateway.rpsls.messaging;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.websockets.WebSocketInMessage;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.serialization.WebSocketInMessageDecoder;
import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.serialization.WebSocketOutMessageEncoder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/socket/rpsls/{clientUUID}/{username}",
        decoders = {WebSocketInMessageDecoder.class},
        encoders = {WebSocketOutMessageEncoder.class}
)
@ApplicationScoped
public class RPSLSSocket {

    @Inject
    RPSLSFacade rpslsFacade;

    @OnOpen
    public void onOpen(Session session, @PathParam("clientUUID") String clientUUID,
                       @PathParam("username") String username) {

        rpslsFacade.openNewSession(session, clientUUID, username);
    }

    @OnClose
    public void onClose(@PathParam("clientUUID") String clientUUID,
                        @PathParam("username") String username) {

        rpslsFacade.cleanUpTerminatedOrCancelledSession(clientUUID, username);
    }

    @OnError
    public void onError(Session session, @PathParam("clientUUID") String clientUUID,
                        @PathParam("username") String username, Throwable throwable) {

        //rpslsFacade.sendSessionCancelledMessage(clientUUID, username);
        //rpslsFacade.cleanUpTerminatedOrCancelledSession(clientUUID, username);
    }

    @OnMessage
    public void onMessage(WebSocketInMessage message) {
        switch (message.header) {
            case CHOOSE:
                rpslsFacade.handleChooseMoveUserMessage(message);
                break;
        }
    }
}
