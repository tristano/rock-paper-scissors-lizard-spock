package nl.ordina.rpslsworkshop.apigateway.common;

import javax.websocket.Session;

public class SessionKeySession {
    private SessionKey sessionKey;
    private Session session;

    public SessionKeySession(SessionKey sessionKey, Session session) {
        this.sessionKey = sessionKey;
        this.session = session;
    }

    public SessionKey getSessionKey() {
        return sessionKey;
    }

    public Session getSession() {
        return session;
    }
}
