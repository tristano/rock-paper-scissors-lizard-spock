package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import nl.ordina.rpslsworkshop.apigateway.common.SessionKey;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.Session;
import java.util.Objects;

@ApplicationScoped
public class OddsEvensFacade {

    @Inject
	OddsEvensWebSocketSessionsContainer webSocketSessionsContainer;

    @Inject
    @RestClient
	OddsEvensGameEngineService evenOddsGameEngineService;

    public void openNewSession(Session session, String clientUUID, String username) {
		webSocketSessionsContainer.addSession(clientUUID, username, session);
    	OddsEvensDTOs.Player player = new OddsEvensDTOs.Player();
    	player.name = username;
    	player.clientUUID = clientUUID;
		evenOddsGameEngineService.registerPlayerToSession(player);
    }

    public void cleanUpTerminatedOrCancelledSession(String clientUUID, String username) {
        evenOddsGameEngineService.getAll(clientUUID, username).stream().findFirst()
                .ifPresent(sessionDTO -> {
                    evenOddsGameEngineService.cancelSession(sessionDTO.sessionUUID);
                });
        webSocketSessionsContainer.removeSession(clientUUID, username);
    }

    public void handleChooseMoveUserMessage(OddsEvensDTOs.WebSocketInMessage message) {
		OddsEvensDTOs.Choice choice = message.payload;
		evenOddsGameEngineService.sendChoice(choice, choice.sessionUUID);
    }

    public void handleNewRoundGameEvent(OddsEvensDTOs.GameEvent gameEvent) {
		handleGameEvent(gameEvent, OddsEvensDTOs.WebSocketOutMessage.Header.NEW_ROUND);
	}

	public void handleSessionTerminatedGameEvent(OddsEvensDTOs.GameEvent gameEvent) {
		handleGameEvent(gameEvent, OddsEvensDTOs.WebSocketOutMessage.Header.SESSION_TERMINATED);
	}

	public void handleSessionCancelledGameEvent(OddsEvensDTOs.GameEvent gameEvent) {
		handleGameEvent(gameEvent, OddsEvensDTOs.WebSocketOutMessage.Header.SESSION_CANCELLED);
	}

	private void handleGameEvent(OddsEvensDTOs.GameEvent gameEvent, OddsEvensDTOs.WebSocketOutMessage.Header header) {
		gameEvent.recipients.stream()
				.filter(Objects::nonNull)
				.forEach(player -> {
					SessionKey sessionKey = new SessionKey(player.clientUUID, player.name);
					if (webSocketSessionsContainer.isSessionInThisInstance(sessionKey)) {
						OddsEvensDTOs.WebSocketOutMessage webSocketOutMessage = new OddsEvensDTOs.WebSocketOutMessage();
						OddsEvensDTOs.Score payload = new OddsEvensDTOs.Score();
						if (gameEvent.score.player1 != null) {
							OddsEvensDTOs.PlayerWithMove player1 = new OddsEvensDTOs.PlayerWithMove();
							player1.name = gameEvent.score.player1.name;
							player1.score = gameEvent.score.player1.score;
							player1.move = gameEvent.score.player1.move;
							if (player.clientUUID.equals(gameEvent.score.player1.clientUUID)) {
								player1.clientUUID = gameEvent.score.player1.clientUUID;
							}
							payload.player1 = player1;
						}
						if (gameEvent.score.player2 != null) {
							OddsEvensDTOs.PlayerWithMove player2 = new OddsEvensDTOs.PlayerWithMove();
							player2.name = gameEvent.score.player2.name;
							player2.score = gameEvent.score.player2.score;
							player2.move = gameEvent.score.player2.move;
							if (player.clientUUID.equals(gameEvent.score.player2.clientUUID)) {
								player2.clientUUID = gameEvent.score.player2.clientUUID;
							}
							payload.player2 = player2;
						}
						webSocketOutMessage.header = header;
						webSocketOutMessage.payload = payload;
						webSocketOutMessage.sessionUUID = gameEvent.sessionUUID;
						webSocketSessionsContainer.send(sessionKey, webSocketOutMessage);
					}
		});
	}
}
