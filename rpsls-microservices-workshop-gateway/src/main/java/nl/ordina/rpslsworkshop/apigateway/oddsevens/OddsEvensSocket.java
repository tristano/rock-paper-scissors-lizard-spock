package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/socket/oe/{clientUUID}/{username}",
		decoders = {WebSocketInMessageDecoder.class},
		encoders = {WebSocketOutMessageEncoder.class}
)
@ApplicationScoped
public class OddsEvensSocket {

	@Inject
	OddsEvensFacade evenOddsFacade;

	@OnOpen
	public void onOpen(Session session, @PathParam("clientUUID") String clientUUID,
					   @PathParam("username") String username) {

		evenOddsFacade.openNewSession(session, clientUUID, username);
	}

	@OnClose
	public void onClose(@PathParam("clientUUID") String clientUUID,
						@PathParam("username") String username) {

		evenOddsFacade.cleanUpTerminatedOrCancelledSession(clientUUID, username);
	}

	@OnError
	public void onError(Session session, @PathParam("clientUUID") String clientUUID,
						@PathParam("username") String username, Throwable throwable) {

		//evenOddsFacade.sendSessionCancelledMessage(clientUUID, username);
		//evenOddsFacade.cleanUpTerminatedOrCancelledSession(clientUUID, username);
	}

	@OnMessage
	public void onMessage(OddsEvensDTOs.WebSocketInMessage message) {
		switch (message.header) {
			case CHOOSE:
				evenOddsFacade.handleChooseMoveUserMessage(message);
				break;
		}
	}
}
