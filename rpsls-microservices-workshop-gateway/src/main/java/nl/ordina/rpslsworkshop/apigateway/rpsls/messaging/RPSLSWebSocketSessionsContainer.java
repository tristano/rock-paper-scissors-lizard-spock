package nl.ordina.rpslsworkshop.apigateway.rpsls.messaging;

import nl.ordina.rpslsworkshop.apigateway.common.WebSocketSessionsContainer;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RPSLSWebSocketSessionsContainer extends WebSocketSessionsContainer {
}
