package nl.ordina.rpslsworkshop.apigateway.oddsevens;

import nl.ordina.rpslsworkshop.apigateway.common.WebSocketSessionsContainer;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class OddsEvensWebSocketSessionsContainer extends WebSocketSessionsContainer {
}
