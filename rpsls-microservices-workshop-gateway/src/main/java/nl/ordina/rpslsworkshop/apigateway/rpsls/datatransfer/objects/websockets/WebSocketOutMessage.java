package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.websockets;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects.Score;

public class WebSocketOutMessage {
    public Header header;
    public Score payload;
    public String sessionUUID;

    public enum Header {
        NEW_ROUND, SESSION_TERMINATED, SESSION_CANCELLED
    }
}
