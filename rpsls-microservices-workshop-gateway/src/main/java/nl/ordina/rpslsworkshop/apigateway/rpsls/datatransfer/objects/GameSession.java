package nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.objects;

import nl.ordina.rpslsworkshop.apigateway.rpsls.datatransfer.GameSessionStatus;

public class GameSession {
    public String sessionUUID;
    public Player player1;
    public Player player2;
    public GameSessionStatus status;
    public Round roundInProgress;
    public Result resultLastRound;
}
